import csv
import json
import os
import pickle
import sys
import datetime
# import subprocess
import pandas
import numpy

import ROOT as r
from dataPrep_corr.makeBeamBeamFile import doMakeBeamBeamFile
from dataPrep_corr.makeDynamicBetaFile import doMakeDynamicBetaFile
from dataPrep_corr.makeGhostsFile import doMakeGhostsFile
from dataPrep_corr.makeLengthScaleFile import doMakeLengthScaleFile
from dataPrep_corr.makeOrbitDriftFile import doMakeOrbitDriftFile
from dataPrep_corr.makeSatellitesFile import doMakeSatellitesFile
from dataPrepII.makeRateFile import doMakeRateFile
from dataPrep_corr.makeBackgroundFile import MakeBackgroundFile
from dataPrepII_PCC.makePCCRateFile_vdm import doMakePCCRateFile_vdm
from dataPrepII_PCC.makePCCRateFile_muscan import doMakePCCRateFile_muscan
from dataPrepII_RAMSES.makeRAMSESRateFile import doMakeRAMSESRateFile
from makeBeamCurrentFileII import doMakeBeamCurrentFile
from makeGraphs2D import doMakeGraphs2D
from makeGraphsFileII import doMakeGraphsFile
from makeScanFileII import doMakeScanFile
from vdmFitterII import doRunVdmFitter
from calculateCalibrationConstant import CalculateCalibrationConstant
from vdmUtilities import setupDirStructure
from calculateEmittance import CalculateEmittance

def DriveVdm(ConfigFile):
    r.gROOT.SetBatch(r.kTRUE)

    Config=open(ConfigFile)
    ConfigInfo = json.load(Config)
    Config.close()


    Fill = str(ConfigInfo['Fill'])
    Date = str(ConfigInfo['Date'])
    Luminometer = str(ConfigInfo['Luminometer'])
    AnalysisDir = str(ConfigInfo['AnalysisDir'])    

    Corr = ConfigInfo['Corr']

    makeScanFile = False
    makeScanFile = ConfigInfo['makeScanFile']

    makeRateFile = False
    makeRateFile = ConfigInfo['makeRateFile']

    makeBeamCurrentFile = False
    makeBeamCurrentFile = ConfigInfo['makeBeamCurrentFile']

    makeBeamBeamFile = False
    makeBeamBeamFile =  ConfigInfo['makeBeamBeamFile']

    makeDynamicBetaFile = False
    makeDynamicBetaFile=ConfigInfo['makeDynamicBetaFile']

    makeGhostsFile =  False
    makeGhostsFile =  ConfigInfo['makeGhostsFile']

    makeSatellitesFile = False
    makeSatellitesFile = ConfigInfo['makeSatellitesFile'] 

    makeLengthScaleFile = False
    makeLengthScaleFile = ConfigInfo['makeLengthScaleFile']
    
    makeOrbitDriftFile = False
    makeOrbitDriftFile = ConfigInfo['makeOrbitDriftFile']

    makeBackgroundFile = False
    makeBackgroundFile = ConfigInfo['makeBackgroundFile']

    makeGraphsFile = False
    makeGraphsFile = ConfigInfo['makeGraphsFile']

    makeGraphs2D = False
    makeGraphs2D = ConfigInfo['makeGraphs2D']

    runVdmFitter = False
    runVdmFitter = ConfigInfo['runVdmFitter']

    calculateCalibrationConstant = False
    calculateCalibrationConstant = ConfigInfo['calculateCalibrationConstant']

    calculateEmittance = False
    calculateEmittance = ConfigInfo['calculateEmittance']


    makepdf = ConfigInfo['MakePDF']
    makelogs = ConfigInfo['MakeLogs']

    muscan = ConfigInfo['makeScanFileConfig']['MuScan']

    fom = ConfigInfo['makeRateFileConfig']['FOM']  

    fit = ConfigInfo['vdmFitterConfig']['FitName']

    print ""
    print "Running with this config info:"
    print "Fill ", Fill
    print "Date ", Date
    print "Luminometer ", Luminometer
    print "Fit", fit
    print "AnalysisDir ", AnalysisDir
    print "Corrections ", Corr
    print "makeScanFile ", makeScanFile
    print "makeBeamCurrentFile ", makeBeamCurrentFile
    print "makeGraphsFile ", makeGraphsFile
    print "runVdmFitter ", runVdmFitter
    print "calculateCalibrationConstant ", calculateCalibrationConstant
    print "calculateEmittance ", calculateEmittance	


    print ""
    setupDirStructure(AnalysisDir, Luminometer, Corr)
    print ""


    ### Scan file
    makeScanFileConfig = ConfigInfo['makeScanFileConfig']

    print "Running makeScanFile with config info:"
    for key in makeScanFileConfig:
        print key, makeScanFileConfig[key]
    print ""

    makeScanFileConfig['Fill'] = Fill
    makeScanFileConfig['Date'] = Date

    OutputSubDir = str(makeScanFileConfig['OutputSubDir'])    
    outpath = './' + AnalysisDir + '/'+ OutputSubDir 

    outputScanFile = outpath+'/'

    if makeScanFile or not (os.path.exists(outpath+'/Scan_'+str(Fill)+'.json') or os.path.exists(outpath+'/Scan_'+str(Fill)+'.pkl')):
        table = {}
        csvtable = []

        table, csvtable = doMakeScanFile(makeScanFileConfig)

        csvfile = open(outpath+'/Scan_'+str(Fill)+'.csv', 'wb')
        writer = csv.writer(csvfile)
        writer.writerows(csvtable)
        csvfile.close()

        with open(outpath+'/Scan_'+str(Fill)+'.json', 'wb') as f:
            json.dump(table, f)
    else:
        print "Attention, previous version of ScanFile will be used"


    ### Rate file
    makeRateFileConfig = ConfigInfo['makeRateFileConfig']
    makeRateFileConfig['Fill'] = Fill
    makeRateFileConfig['AnalysisDir'] = AnalysisDir

    OutputSubDir = AnalysisDir + "/" + str(makeRateFileConfig['OutputSubDir'])

    if (makeRateFile
        or ( not fom==True and not os.path.exists(OutputSubDir+'/Rates_' + Luminometer + '_' + str(Fill) + '.json'))
	or ( fom==True and not os.path.exists(OutputSubDir+'/FOM_Rates_' + Luminometer + '_' + str(Fill) + '.json'))):
        table = {}

        print "\nRunning makeRateFile with config info:"
        for key in makeRateFileConfig:
            print key, makeRateFileConfig[key]
        print ""
	checkRateFlag=0
        if Luminometer=='PCC' or Luminometer=='VTX':
	    if Luminometer=='PCC' and os.path.exists(OutputSubDir+'/Rates_VTX_' + str(Fill) + '.json'):
		with open(OutputSubDir+'/Rates_VTX_' + str(Fill) + '.json') as f:
		    checkRate=json.load(f)
		checkRateFlag=1
	    elif Luminometer=='VTX' and os.path.exists(OutputSubDir+'/Rates_PCC_' + str(Fill) + '.json'):
		with open(OutputSubDir+'/Rates_PCC_' + str(Fill) + '.json') as f:
                    checkRate=json.load(f)
		checkRateFlag=1
	    if checkRateFlag==0:
	    	checkRateCount=1
	    elif checkRateFlag==1:
		checkRateCount=0
	    while True:
		if checkRateCount>1:
		    break
	        if muscan==True:
	    	    table, csvtable = doMakePCCRateFile_muscan(makeRateFileConfig)
	        else:
            	    #print "makeRateFile skipped"
	    	    table, csvtable = doMakePCCRateFile_vdm(makeRateFileConfig)
	        #Sanity check for the rate files of VTX and PCC
		if checkRateFlag==1:
		    if checkRate["Scan_1"][0]["RateErrs"].values()[0] != table["Scan_1"][0]["RateErrs"].values()[0]:
		        checkRateFlag=0
			break
		checkRateCount+=1
	elif  Luminometer.startswith('RAMSES'):
	    table = doMakeRAMSESRateFile(makeRateFileConfig)
        else:
            table = doMakeRateFile(makeRateFileConfig)
	if checkRateFlag==1:
	    print "ERROR! PCC and VTX rate files created as the same so "+Luminometer+" rate file have not be saved! Check the other rate file and delete it if the rates seem so high or so small. After deletion rerun the framework for PCC and VTX! "
	else:
	    if fom==True:
		with open(OutputSubDir+'/FOM_Rates_' + Luminometer +  '_'+str(Fill)+'.json', 'wb') as f:
		    json.dump(table, f)
	    else:
                with open(OutputSubDir+'/Rates_' + Luminometer +  '_'+str(Fill)+'.json', 'wb') as f:
                    json.dump(table, f)


    ### Beam current files
    makeBeamCurrentFileConfig = ConfigInfo['makeBeamCurrentFileConfig']
    makeBeamCurrentFileConfig['AnalysisDir'] = AnalysisDir

    OutputSubDir = str(makeBeamCurrentFileConfig['OutputSubDir'])
    outpath = './' + AnalysisDir + '/' + OutputSubDir

    if (makeBeamCurrentFile or
       not (os.path.exists(outpath+'/BeamCurrents_'+str(Fill)+'.json')
            or os.path.exists(outpath + '/BeamCurrents_' +str(Fill)+'.pkl'))):
        table = {}

        print "Running makeBeamCurrentFile with config info:"
        for key in makeBeamCurrentFileConfig:
            print key, makeBeamCurrentFileConfig[key]
        print ""

        table = doMakeBeamCurrentFile(makeBeamCurrentFileConfig)

        with open(outpath+'/BeamCurrents_'+str(Fill)+'.json', 'wb') as f :
            json.dump(table,f)

    ### BeamBeam correction file
    if makeBeamBeamFile == True:

        makeBeamBeamFileConfig = ConfigInfo['makeBeamBeamFileConfig']
        print "Running makeBeamBeamFile with config info:"
        for key in makeBeamBeamFileConfig:
            print key, makeBeamBeamFileConfig[key]
        print ""

        makeBeamBeamFileConfig['AnalysisDir'] = AnalysisDir
        makeBeamBeamFileConfig['Fill'] =  Fill
        makeBeamBeamFileConfig['Luminometer'] =  Luminometer
        if fit[-1] == 'S':            
            makeBeamBeamFileConfig['Luminometer'] =  'PLT'
            makeBeamBeamFileConfig['InputCapSigmaFile'] = "noCorr/DG_FitResults.pkl"
        OutputDir = AnalysisDir +'/'+makeBeamBeamFileConfig['OutputSubDir']

        table = {}
        csvtable=[]

        table, csvtable = doMakeBeamBeamFile(makeBeamBeamFileConfig)

	isFOM='FOM_' if fom==True else ''	

        with open(OutputDir+'/'+isFOM+'BeamBeam_'+ Luminometer + '_' +str(Fill)+'.json', 'wb') as f:
            json.dump(table, f)

        csvfile = open(OutputDir+'/'+isFOM+'BeamBeam_' + Luminometer + '_' + str(Fill)+'.csv', 'wb')
        writer = csv.writer(csvfile)
        writer.writerows(csvtable)
        csvfile.close()

    ### DynamicBeta correction file
    if makeDynamicBetaFile==True:

        makeDynamicBetaFileConfig=ConfigInfo['makeDynamicBetaFileConfig']
        print "Running makeDynamicBetaFile with config info:"
        for key in makeDynamicBetaFileConfig:
            print key, makeDynamicBetaFileConfig[key]
        print ""

        makeDynamicBetaFileConfig['Fill']=Fill
        makeDynamicBetaFileConfig['AnalysisDir']=AnalysisDir
        makeDynamicBetaFileConfig['Luminometer']=Luminometer
        OutputDir=AnalysisDir + '/' + makeDynamicBetaFileConfig['OutputSubDir']
        BaseTableFile="dataPrep_corr/DynamicBetaInfo/dynBetaCrctnTable_v1.0_22May13.txt"

        table={}
        csvtable=[]

        table, csvtable=doMakeDynamicBetaFile(makeDynamicBetaFileConfig,BaseTableFile)

	isFOM='FOM_' if fom==True else ''

        with open(OutputDir+'/'+isFOM+'DynamicBeta_'+Luminometer+'_'+str(Fill)+'.json','wb') as f:
            json.dump(table,f)

        csvfile=open(OutputDir+'/'+isFOM+'DynamicBeta_'+Luminometer+'_'+str(Fill)+'.csv','wb')
        writer=csv.writer(csvfile)
        writer.writerows(csvtable)
        csvfile.close()

    if makeGhostsFile == True:

        makeGhostsFileConfig = ConfigInfo['makeGhostsFileConfig']

        print "Running makeGhostsFile with config info:"
        for key in makeGhostsFileConfig:
            print key, makeGhostsFileConfig[key]
        print ""

        makeGhostsFileConfig['AnalysisDir'] = AnalysisDir
        makeGhostsFileConfig['Fill'] = Fill

        OutputDir = AnalysisDir +'/'+ makeGhostsFileConfig['OutputSubDir']

        table = {}
        csvtable = []
        table, csvtable = doMakeGhostsFile(makeGhostsFileConfig)

        csvfile = open(OutputDir+'/Ghosts_'+str(Fill)+'.csv', 'wb')
        writer = csv.writer(csvfile)
        writer.writerows(csvtable)
        csvfile.close()

        with open(OutputDir+'/Ghosts_'+str(Fill)+'.pkl', 'wb') as f:
            pickle.dump(table, f)


    if makeSatellitesFile == True:

        makeSatellitesFileConfig = ConfigInfo['makeSatellitesFileConfig']

        print "Running makeSatellitesFile with config info:"
        for key in makeSatellitesFileConfig:
            print key, makeSatellitesFileConfig[key]
        print ""

        makeSatellitesFileConfig['AnalysisDir'] = AnalysisDir
        makeSatellitesFileConfig['Fill'] = Fill

        OutputDir = AnalysisDir +'/'+ makeSatellitesFileConfig['OutputSubDir']

        table = {}
        csvtable = []
        table, csvtable = doMakeSatellitesFile(makeSatellitesFileConfig)

        csvfile = open(OutputDir+'/Satellites_'+str(Fill)+'.csv', 'wb')
        writer = csv.writer(csvfile)
        writer.writerows(csvtable)
        csvfile.close()

        with open(OutputDir+'/Satellites_'+str(Fill)+'.pkl', 'wb') as f:
            pickle.dump(table, f)


    if makeLengthScaleFile == True:

        makeLengthScaleFileConfig = ConfigInfo['makeLengthScaleFileConfig']

        print "Running makeLengthScaleFile with config info:"
        for key in makeLengthScaleFileConfig:
            print key, makeLengthScaleFileConfig[key]
        print ""

        makeLengthScaleFileConfig['AnalysisDir'] = AnalysisDir
        makeLengthScaleFileConfig['Fill'] = Fill

        OutputDir = AnalysisDir +'/'+ makeLengthScaleFileConfig['OutputSubDir']

        table = {}
        csvtable = []
        table, csvtable = doMakeLengthScaleFile(makeLengthScaleFileConfig)

        csvfile = open(OutputDir+'/LengthScale_'+str(Fill)+'.csv', 'wb')
        writer = csv.writer(csvfile)
        writer.writerows(csvtable)
        csvfile.close()

        with open(OutputDir+'/LengthScale_'+str(Fill)+'.pkl', 'wb') as f:
            pickle.dump(table, f)

    
    if makeOrbitDriftFile == True:
    
        makeOrbitDriftFileConfig = ConfigInfo['makeOrbitDriftFileConfig']

        print "Running makeOrbitDriftFile with config info:"
        for key in makeOrbitDriftFileConfig:
            print key, makeOrbitDriftFileConfig[key]
        print ""

        makeOrbitDriftFileConfig['AnalysisDir'] = AnalysisDir
        makeOrbitDriftFileConfig['Fill'] = Fill

        OutputDir = AnalysisDir +'/'+ makeOrbitDriftFileConfig['OutputSubDir']

        table = {}
        csvtable = []
        table, csvtable = doMakeOrbitDriftFile(makeOrbitDriftFileConfig)

        csvfile = open(OutputDir+'/OrbitDrift_'+str(Fill)+'.csv', 'wb')
        writer = csv.writer(csvfile)
        writer.writerows(csvtable)
        csvfile.close()

        with open(OutputDir+'/OrbitDrift_'+str(Fill)+'.pkl', 'wb') as f:
            pickle.dump(table, f)
    
    
    if makeBackgroundFile == True:
        makeBackgroundFileConfig = {}
        makeBackgroundFileConfig['RateTable'] = ConfigInfo['makeBackgroundFileConfig']['RateTable']
        makeBackgroundFileConfig['Filename'] = ConfigInfo['makeBackgroundFileConfig']['Filename']
	makeBackgroundFileConfig['Filename2'] = ConfigInfo['makeBackgroundFileConfig']['Filename2']
	makeBackgroundFileConfig['AnalysisDir'] = ConfigInfo['AnalysisDir']
	makeBackgroundFileConfig['Fixed'] = json.loads(ConfigInfo['makeBackgroundFileConfig']['FixedBackground'])
	makeBackgroundFileConfig['SuperSeparation'] = ConfigInfo['makeBackgroundFileConfig']['SuperSeparation']
	print "Running makeBackgroundFile with config info:"
        for key in makeBackgroundFileConfig:
            print key, makeBackgroundFileConfig[key]
        print ""
        background = MakeBackgroundFile(makeBackgroundFileConfig)

        with open(AnalysisDir + '/corr/Background_' + Luminometer + '_' + str(Fill) + '.json', 'wb') as f:
            json.dump(background, f)

    if makeGraphsFile == True:

        makeGraphsFileConfig = ConfigInfo['makeGraphsFileConfig']

        print "\nRunning makeGraphsFile with config info:"
        for key in makeGraphsFileConfig:
            print key, makeGraphsFileConfig[key]
        print ""

        makeGraphsFileConfig['AnalysisDir'] = AnalysisDir
        makeGraphsFileConfig['Luminometer'] = Luminometer
        makeGraphsFileConfig['Fill'] = Fill
        makeGraphsFileConfig['Corr'] = Corr
        if fit[-1] == 'S':  
            makeGraphsFileConfig['inputlumi'] = 'PLT'

        graphsListAll = {}

        corrFull, graphs, missedDataBuffer = doMakeGraphsFile(makeGraphsFileConfig)

        OutputSubDir = str(makeGraphsFileConfig['OutputSubDir'])
        OutputDir = AnalysisDir +'/' + Luminometer + '/' + OutputSubDir + '/'
        outFileName = 'graphs_' + str(Fill) + corrFull
	if fom==True:
	    outFileName = 'FOM_graphs_' + str(Fill) + corrFull

        with open(OutputDir + outFileName + '.json', 'wb') as f:
            json.dump(graphs,f)

        misseddata=open(OutputDir+"makeGraphsFile_MissedData.log",'w')
        misseddata.write(missedDataBuffer)
        misseddata.close()

        print '\nmakeGraphsFile finished.\n'

    ## PT: Have no idea if this works, haven't needed it and so haven't updated
    if makeGraphs2D == True:

        makeGraphs2DConfig = ConfigInfo['makeGraphs2DConfig']

        print "Running makeGraphs2D with config info:"
        for key in makeGraphs2DConfig:
            print key, makeGraphs2DConfig[key]
        print ""

        makeGraphs2DConfig['AnalysisDir'] = AnalysisDir
        makeGraphs2DConfig['Luminometer'] = Luminometer
        makeGraphs2DConfig['Fill'] = Fill
        makeGraphs2DConfig['Corr'] = Corr

        graphs2DListAll = {}

        corrFull, graphs2DListAll = doMakeGraphs2D(makeGraphs2DConfig)

        InOutSubDir = makeGraphs2DConfig['InOutSubDir']
        outputDir = AnalysisDir + '/' + Luminometer + '/' + InOutSubDir + '/'

        #2D graph file should be called graphs2D_<n>_<corrFull>.pkl
        GraphFile2D = outputDir + 'graphs2D_' + Fill + corrFull + '.pkl'

        file2D = open(GraphFile2D, 'wb')
        pickle.dump(graphs2DListAll, file2D)
        file2D.close()


    if runVdmFitter == True:

        vdmFitterConfig = ConfigInfo['vdmFitterConfig']

        print "\nRunning runVdmFitter with config info:"
        for key in vdmFitterConfig:
            print key, vdmFitterConfig[key]
        print ""

        vdmFitterConfig['AnalysisDir'] = AnalysisDir
        vdmFitterConfig['Luminometer'] = Luminometer
        vdmFitterConfig['Fill'] = Fill
        vdmFitterConfig['Corr'] = Corr

        FitName = vdmFitterConfig['FitName']
        FitConfigFile = vdmFitterConfig['FitConfigFile']
        PlotsTempPath = [["./" + AnalysisDir + '/' + Luminometer + '/' + "plotstmp/"]]

        corrFull = ""
        for entry in Corr:
            corrFull = corrFull + '_' + str(entry)
        if corrFull[:1] == '_':
            corrFull = corrFull[1:]
        if  not corrFull:
            corrFull = "noCorr"
        InputGraphsFiles = []
        OutputDirs = []
        if 'InputGraphsFile' in vdmFitterConfig:
	    InputGraphsFile = AnalysisDir + '/' + Luminometer + '/' + vdmFitterConfig['InputGraphsFile']
	    if fom==True:
                InputGraphsFile = AnalysisDir + '/' + Luminometer + '/' + vdmFitterConfig['FOM_InputGraphsFile']
            if (corrFull not in InputGraphsFile):
                raw_input("InputGraphsFile extension different than the Correction to be applied!!; Press ENTER to continue.")
            InputGraphsFiles.append(InputGraphsFile)
        else:
            defaultGraphsFile = 'graphs' + '/' + 'graphs_' + Fill + '_' + corrFull + '.pkl'
            InputGraphsFile = AnalysisDir + '/' + Luminometer + '/' +  defaultGraphsFile
            InputGraphsFiles.append(InputGraphsFile)

        OutputDir = './' + AnalysisDir + '/' + Luminometer + '/results/' + corrFull + '/'
        OutputDirs.append(OutputDir)

        # PT: Have no idea if this works, haven't needed it and so haven't updated
        # CP:  thanks...
	# YC: zzzzzz....
        if 'Sim' in FitConfigFile:
            #PlotsTempPath = vdmFitterConfig['PlotsTempPath']
	    PlotsTempPath = [["./" + AnalysisDir + '/' + Luminometer + '/' + "plotstmp/"],["./" + AnalysisDir + '/' + 'VTX' + '/' + "plotstmp/"]]
            if 'InputSimGraphsFile' in vdmFitterConfig:
                InputSimGraphsFile = AnalysisDir + '/' + 'VTX' + '/' + vdmFitterConfig['InputSimGraphsFile']
                InputGraphsFiles.append(InputSimGraphsFile)
            else:
                defaultSimGraphsFile = 'graphs' + '/' + 'graphs_' + Fill + '_' + corrFull + '.json'
                InputSimGraphsFile = AnalysisDir + '/' + 'VTX' + '/' +  defaultSimGraphsFile
                InputGraphsFiles.append(InputSimGraphsFile)
            OutputDir = './' + AnalysisDir + '/' + 'VTX' + '/results/' + corrFull + '/'
            OutputDirs.append(OutputDir)

        print "ATTENTION: Output will be written into ", OutputDirs[0]

        FitConfig=open(FitConfigFile)
        FitConfigInfo = json.load(FitConfig)
        FitConfig.close()

        FitConfigInfo['MakeLogs'] = makelogs
        # minuit logs path resolving
        if makelogs:
            MinuitLogPath = "./" + AnalysisDir + '/' + Luminometer + '/minuitlog/'
            MinuitLogFile = MinuitLogPath + vdmFitterConfig['MinuitFile'] + datetime.datetime.now().strftime('%y%m%d_%H%M%S') + '.log'
            if not os.path.isdir(MinuitLogPath):
                os.mkdir(MinuitLogPath, 0755)

            FitConfigInfo['MinuitFile'] = MinuitLogFile

        for path in PlotsTempPath:
            if not os.path.isdir(path[0]):
                os.makedirs(path[0], 0755)
            else:
                filelist = os.listdir(path[0])
                for element in filelist:
                    if ('ps' or 'root') in element:
                        os.remove(path[0]+element)

        resultsAll = {}
        table = []

        resultsAll, table = doRunVdmFitter(Fill, FitName, InputGraphsFiles, OutputDirs[0], PlotsTempPath,
                                           FitConfigInfo, AnalysisDir, makepdf=makepdf, makelogs=makelogs)

        for (i,OutputDir) in enumerate(OutputDirs):
	    isFOM='FOM_' if fom==True else ''
	    print OutputDir
            outResults ='./'+ OutputDir + '/'+isFOM+FitName+'_FitResults.pkl'
            outFile = open(outResults, 'wb')
            pickle.dump(table[i], outFile)
            outFile.close()

            outputFitResultsFile='./'+ OutputDir + '/'+isFOM+FitName+'_FitResults.csv'
            csvfile = open('./'+ OutputDir + '/'+isFOM+FitName+'_FitResults.csv', 'wb')
            writer = csv.writer(csvfile)
            writer.writerows(table[i])
            csvfile.close()
        
            # outResults ='./'+ OutputDir + '/'+FitName+'_Functions.pkl'
            # outFile = open(outResults, 'wb')
            # pickle.dump(resultsAll, outFile)
            # outFile.close()


        # PDF shapes output, removing temp plots
        output_FittedGraphs = dict(zip(OutputDirs,PlotsTempPath))
        for OutputDir in output_FittedGraphs:
	    isFOM='FOM_' if fom==True else ''
            outPdf = './'+OutputDir + '/'+isFOM+FitName+'_FittedGraphs.pdf'
            PlotsPath = output_FittedGraphs[OutputDir][0]
            filelist = os.listdir(PlotsPath)
            merge =-999.
            for element in filelist:
                if element.find(".ps") > 0:
                    merge = +1.
            if merge > 0:
                os.system("gs -dNOPAUSE -sDEVICE=pdfwrite -dBATCH -q -sOutputFile="+outPdf+" " + PlotsPath+"/*.ps")

            #NOTE delete this if nothing breaks
            # outRoot = './'+OutputDir + '/'+FitName+'_FittedGraphs.root'
            # if os.path.isfile(outRoot):
            #     os.remove(outRoot)

            # merge =-999.
            # for element in filelist:
            #     if element.find(".root") > 0:
            #        merge = +1.
            # if merge > 0:
            #     with open(os.devnull,'wb') as devnull:
            #         return_code = subprocess.call(["hadd",outRoot,PlotsPath + "*.root"], stdout=devnull)
            #         if return_code:
            #             raise Exception("ROOT hadd failed, pdf may not be complete")
            os.system('rm ' + PlotsPath + '/*')
        print('\nVdM Fitter finished.\n')
  
    if calculateCalibrationConstant:
        calculateCalibrationConstantConfig = ConfigInfo['calculateCalibrationConstantConfig']

        print('\nRunning calculateCalibrationConstant with config info:')
        for key in calculateCalibrationConstantConfig:
            # print key
            print(key + ' ' + str(calculateCalibrationConstantConfig[key]))

        calibration = CalculateCalibrationConstant(calculateCalibrationConstantConfig)
        print('\nCalculateCalibrationConstant finished.\n')

    if calculateEmittance:
	
        calculateEmittanceConfig=ConfigInfo['calculateEmittanceConfig']        
  	beamlength, emittance, sigma = CalculateEmittance(calculateEmittanceConfig, outputFitResultsFile)
	#insertion of the column into "FitResults.csv" file
        DataFrame=pandas.read_csv(outputFitResultsFile)
        DataFrame.insert(DataFrame.columns.get_loc("CapSigma"),'Emittance', emittance)
	DataFrame['sigma']=sigma
        DataFrame.to_csv(outputFitResultsFile, index=False)

   	if not beamlength.empty:
	    if not os.path.exists(outputScanFile+"BeamLength.csv"):  
	        beamlength.to_csv(outputScanFile+"BeamLength.csv", index=False)

    if runVdmFitter or CalculateCalibrationConstant:
        return table,calibration

if __name__=='__main__':
    ConfigFile = sys.argv[1]
    DriveVdm(ConfigFile)
