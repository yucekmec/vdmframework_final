import CorrectionManager
import BeamBeam_Corr
import LengthScale_Corr
import OrbitDrift_Corr
import Ghosts_Corr
import Satellites_Corr
import Background_Corr
import DynamicBeta_Corr
import ROOT as r
import sys
import json
import math
import os
import csv

from inputDataReaderII import * 
from vdmUtilities import showAvailableCorrs
import fitResultReader

###################
#check for missed data
def checkScanpointList(vdMData):
    print 'sum' in vdMData.usedCollidingBunches, 'sum' in vdMData.collidingBunches
    BCIDListLength=len(vdMData.usedCollidingBunches)
    SPListLength=vdMData.nSP
    SPList=vdMData.displacement
    missedData="BCID:list of missed scanpoints\n"
    
    missedSP=[[] for a in range(BCIDListLength)]
    for i, bx in enumerate(vdMData.usedCollidingBunches):
        for j, sp in enumerate(SPList):
            if sp not in vdMData.spPerBX[bx]:
                missedSP[i].append(j+1)

    for i, bx in enumerate(vdMData.usedCollidingBunches):
        length=len(missedSP[i])
        if length!=0:
            BCID=str(bx)
            missedSPList=str(missedSP[i])
            missedData=missedData+BCID+":"+missedSPList+"\n"

    return missedData

###########################
def doMakeGraphsFile(ConfigInfo):

    AnalysisDir = str(ConfigInfo['AnalysisDir'])
    Luminometer = str(ConfigInfo['Luminometer'])
    inputScanFile = str(ConfigInfo['InputScanFile'])
    inputBeamCurrentFile = str(ConfigInfo['InputBeamCurrentFile'])
    inputLuminometerData = str(ConfigInfo['InputLuminometerData'])
    fom=ConfigInfo['FOM']
    if fom==True:
	inputLuminometerData = str(ConfigInfo['FOM_InputLuminometerData'])
    corrName = ConfigInfo['Corr']
    makepdf = ConfigInfo['MakePDF']
 
    # For scan 1, which is always there as long as there are any scans at all:

    inData1 = vdmInputData(1)

    inData1.GetScanInfo(AnalysisDir + '/'+ inputScanFile)
    #inData1.PrintScanInfo()

    inData1.GetBeamCurrentsInfo(AnalysisDir + '/' + inputBeamCurrentFile)
    #inData1.PrintBeamCurrentsInfo()

    inData1.GetLuminometerData(AnalysisDir + '/' + inputLuminometerData)
    #inData1.PrintLuminometerData()

    Fill = inData1.fill
    
    inData = []
    inData.append(inData1)

    # For the remaining scans:

    for i in range(1,len(inData1.scanNamesAll)):
        inDataNext = vdmInputData(i+1)
        inDataNext.GetScanInfo(AnalysisDir + '/' + inputScanFile)
        inDataNext.GetBeamCurrentsInfo(AnalysisDir + '/' + inputBeamCurrentFile)
        inDataNext.GetLuminometerData(AnalysisDir + '/' + inputLuminometerData)
        inData.append(inDataNext)

    #Check for missing data
    missedDataInfo="Information on missed data\n"
    if "BeamBeam" in corrName:
        missedDataInfo=missedDataInfo+"BeamBeam has been applied: Only BCIDs with complete scanpoint lists are considered\n See BeamBeam_.log for the list of excluded BCID\n"
    else:
        missedDataInfo=missedDataInfo+"BeamBeam has not been applied\n\n"

    for entry in inData:
        scanNumber = entry.scanNumber
        prefix = ''
        if 'X' in entry.scanName:
            prefix = str(scanNumber) +'_X'
        if 'Y' in entry.scanName:
            prefix = str(scanNumber)+'_Y'
        missedDataInfo=missedDataInfo+"Scan_"+prefix+"\n"
        missedSPList=checkScanpointList(entry)
        missedDataInfo=missedDataInfo+missedSPList+"\n"

    # Apply corrections
    # Note that calibrating SumFBCT to DCCT is done in makeBeamCurrentFile.py if calibration flag in config file is set to true

    # showAvailableCorrs()

    availableCorr = CorrectionManager.get_plugins(CorrectionManager.CorrectionProvider)

    # print "The following corrections will be applied, in order: "
    # for i, entry in enumerate(corrName):
    #     print "Corr #"+str(i+1)  + ": " +entry

    corrFull = ''

    for entry in corrName:

        print "Now applying correction: ", entry

        # Check whether correction requested in config json actually exists

        key = entry+'_Corr'
        if key in availableCorr:
            corrector= availableCorr[entry+'_Corr']()        
        else:
            if not entry == "noCorr":
                print "Correction " + entry + " requested via json file does not exist, ignore."
            continue

        # Read Corr config in here

        corrValueFile = AnalysisDir + '/corr/'+ entry + '_' + Fill +'.pkl' 
        if entry == "BeamBeam":
	    corrValueFile = AnalysisDir + '/corr/'+ entry + '_' +Luminometer + '_' + Fill +'.json'
	    if fom==True:
		corrValueFile = AnalysisDir + '/corr/FOM_'+ entry + '_' +Luminometer + '_' + Fill +'.json'
            if 'inputlumi' in ConfigInfo:
                corrValueFile = AnalysisDir + '/corr/'+ entry + '_' + ConfigInfo['inputlumi'] + '_' + Fill +'.json'
        
        if entry == "Background":
            corrValueFile = AnalysisDir + '/corr/'+ entry + '_' + Luminometer + '_' + Fill + '.json'

        if entry=="DynamicBeta":
            corrValueFile = AnalysisDir + '/corr/'+ entry + '_' + Luminometer + '_' + Fill + '.json'
            if fom==True:
                corrValueFile = AnalysisDir + '/corr/FOM_'+ entry + '_' +Luminometer + '_' + Fill +'.json'
        if entry == "BeamBeam":
            corrector.doCorr(inData, corrValueFile, makepdf)
        else:
            corrector.doCorr(inData, corrValueFile)

        corrFull = corrFull + '_' + entry 

    # check if any corrections are to be applied at all, if yes, define corr description string accordingly
    # if no use "noCorr"

    # empty strings are false, so if no corrections are to be applied, use noCorr as corr descriptor
    if  not corrFull:
        corrFull = "_noCorr"


    # Now fill graphs for all scans
    # Counting of scans starts with 1
    graphsListAll = {'Scan_'+ str(n+1):{} for n in range(len(inData))} 

    missedDataInfo=missedDataInfo+"Excluded BCIDs with too short scanpoint list:\n"

    for entry in inData:
        scanNumber = entry.scanNumber
        print "Now at Scan number ", scanNumber
        nBX = len(entry.usedCollidingBunches)
        prefix = ''
        if 'X' in entry.scanName:
            prefix = str(scanNumber) +'_X_'
        if 'Y' in entry.scanName:
            prefix = str(scanNumber)+'_Y_'

        omittedBXList=[]
        # convert for TGraph

        graphsList = {}
	if Luminometer == 'HFOC':
	    sumR_HFOC = []
	    for bx in entry.usedCollidingBunches:
		if not sumR_HFOC:
		    sumR_HFOC = entry.lumiPerBX[bx]
		else:
		    sumR_HFOC =  [ a+b for a,b in zip(entry.lumiPerBX[bx], sumR_HFOC)]
	    sumAlpha=[]
	    sumAlphae=[]
	    for hfbx in entry.collidingBunches:
		hfocbcid=hfbx
		break
        for i, bx in enumerate(entry.usedCollidingBunches):
            # BCIDs written at small number of SP are omitted (the list of short omitted bunches is added to log)            
            # to avoid problems in vdmFitter: the number of SP should exceed the minimal number of freedom degrees for fitting
            if len(entry.spPerBX[bx])>5:
               coord=entry.spPerBX[bx]
               # coorde = [0.0 for a in coord] 
               # coord = array("d",coord)
               # coorde = array("d", coorde)
	           
               currProduct = [ a*b/1e22 for a,b in zip(entry.avrgFbctB1PerBX[bx],entry.avrgFbctB2PerBX[bx])]
	       if Luminometer == 'HFOC':
	           alpha=[ a/(b*c) for a,b,c in zip(entry.lumiPerBX[bx],currProduct, sumR_HFOC)]
		   alphae=[ a/(b*c) for a,b,c in zip(entry.lumiErrPerBX[bx],currProduct, sumR_HFOC)]
		   if not sumAlpha:
		   	sumAlpha = alpha
			sumAlphae = alphae
		   else:
			sumAlpha = [ a+b for a,b in zip(alpha,sumAlpha)]
			sumAlphae = [ a+b for a,b in zip(alphae,sumAlphae)]
	       if Luminometer.startswith('RAMSES'):
		   with open(AnalysisDir+'/cond/SumAlphaHFOC.csv',mode='r') as ahf:
		        ahf_r = csv.reader(ahf, delimiter=',')
			alphaRamses = []
			for row in ahf_r:
			    alphaRamses.append(row)
		   if scanNumber ==1:
		        currProduct=[1/float(a) for a in alphaRamses[0]]
		   elif scanNumber ==2:
                        currProduct=[1/float(a) for a in alphaRamses[1]]
		   '''
		   sumAvrgCollFbct = []
		   for cbx in entry.collidingBunches:
			#print entry.avrgFbctB1PerBX[cbx]
			if not sumAvrgCollFbct:
			    sumAvrgCollFbct = [ a*b for a,b in zip(entry.avrgFbctB1PerBX[bx],entry.avrgFbctB2PerBX[bx])] 
		   	else:
			    temp=[ a*b for a,b in zip(entry.avrgFbctB1PerBX[bx],entry.avrgFbctB2PerBX[bx])]
			    sumAvrgCollFbct = [ a+b for a,b in zip(temp,sumAvrgCollFbct)]
			#print sumAvrgCollFbct, cbx
		   #sumAvrgCollDCCTB1= [ (a-(b-c)) for a,b,c in zip(entry.avrgDcctB1, entry.sumAvrgFbctB1, entry.sumCollAvrgFbctB1)]
		   #sumAvrgCollDCCTB2= [ (a-(b-c)) for a,b,c in zip(entry.avrgDcctB2, entry.sumAvrgFbctB2, entry.sumCollAvrgFbctB2)]
                   currProduct = [ a/1e22 for a in (sumAvrgCollFbct)]
		   '''
	       '''
	       print "------------------------------------------------------------------------------"
	       print "BX:", bx
	       print "sumAvrgCollFbct",sumAvrgCollFbct
	       print "currProduct:", currProduct
	       print "avrgDcctB1:", [ a/1e11 for a in (entry.avrgDcctB1)]
	       print "avrgDcctB2:", [ a/1e11 for a in (entry.avrgDcctB2)]
	       print "DCCTB1*DCCTB2:", [ a*b/1e22 for a,b in zip(entry.avrgDcctB1,entry.avrgDcctB2)]
               print "------------------------------------------------------------------------------"
	       '''
	       #if len(entry.spPerBX[bx])!=len(entry.splumiPerBX[bx]):
               if len(entry.spPerBX[bx])!=len(entry.lumiPerBX[bx]):
                   print "Attention: bx=", bx, ", number of scanpoints for lumi and currents do not match, normalization is not correct"
               
	       lumi = [a/b for a,b in zip(entry.lumiPerBX[bx],currProduct)]
               lumie = [a/b for a,b in zip(entry.lumiErrPerBX[bx],currProduct)]
                   
               #lumi = [a/b for a,b in zip(entry.lumi[i],currProduct)]
               #lumie = [a/b for a,b in zip(entry.lumiErr[i],currProduct)]

               name = prefix +'{:04d}'.format(int(bx))
	       #if Luminometer != 'HFOC':
               graphsList.update({bx:{
                   'name':name,
                   'sep':coord,
                   #'seperr':coorde,
                   'normrate':lumi,
                   'normrateerr':lumie
               }})
            else:
               omittedBXList.append(bx)
	'''
        # same for the sum, as double check, where sumLumi comes from avgraw
        try:
            coord = entry.displacement
            # coorde = [0.0 for a in coord] 
            # coord = array("d",coord)
            # coorde = array("d", coorde)
            currProduct = [ a*b/1e22 for a,b in zip(entry.sumCollAvrgFbctB1,entry.sumCollAvrgFbctB2)]
            lumi = [a/b for a,b in zip(entry.sumLumi,currProduct)]
            lumie = [a/b for a,b in zip(entry.sumLumiErr,currProduct)]
            name = prefix + 'sum'
            graphsList.update({'sum':{
                'name':name,
                'sep':coord,
                # 'seperr':coorde,
                'normrate':lumi,
                'normrateerr':lumie
            }})
        except KeyError,e: 
            print 'KeyError in makeGraphsFile- reason "%s"' % str(e)

	'''
        graphsListAll['Scan_'+ str(scanNumber)]=graphsList
        missedDataInfo=missedDataInfo+"Scan_"+prefix+":"+str(omittedBXList)+"\n" 

        if Luminometer == 'HFOC':
	    lumi = [a*b for a,b in zip(sumAlpha,sumR_HFOC)]
            lumie = [a*b for a,b in zip(sumAlphae,sumR_HFOC)]
	    if scanNumber == 1:
	        with open(AnalysisDir+'/cond/SumAlphaHFOC.csv',mode='wb') as hfr:
                    hfr_w = csv.writer(hfr, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                    hfr_w.writerow(sumAlpha)
		'''
		graphsList.update({hfocbcid:{
                    'name':name,
                    'sep':coord,
                    # 'seperr':coorde,
                    'normrate':lumi,
                    'normrateerr':lumie
                }})
		'''
	    elif scanNumber == 2:
	        with open(AnalysisDir+'/cond/SumAlphaHFOC.csv',mode='a') as hfr:
                    hfr_w = csv.writer(hfr, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                    hfr_w.writerow(sumAlpha)
		    nbx = len(entry.collidingBunches)
		    hfr_w.writerow([nbx])
		'''
	    	graphsList.update({hfocbcid:{
                    'name':name,
                    'sep':coord,
                    # 'seperr':coorde,
                    'normrate':lumi,
                    'normrateerr':lumie
                }})
		'''
    return corrFull, graphsListAll, missedDataInfo

################################################
if __name__ == '__main__':

    configFile = sys.argv[1]

    config=open(configFile)
    ConfigInfo = json.load(config)
    config.close()

    AnalysisDir = str(ConfigInfo['AnalysisDir'])
    Luminometer = str(ConfigInfo['Luminometer'])
    Fill = str(ConfigInfo['Fill'])
    OutputSubDir = str(ConfigInfo['OutputSubDir'])

    graphsListAll = {}

    corrFull, graphsListAll, missedDataBuffer = doMakeGraphsFile(ConfigInfo) 

    outputDir = AnalysisDir +'/' + Luminometer + '/' + OutputSubDir + '/'
    outFileName = 'graphs_' + str(Fill) + corrFull

    # save TGraphs in a ROOT file
    rfile = r.TFile(outputDir + outFileName + '.root',"recreate")

    for key in sorted(graphsListAll.iterkeys()):
        graphsList = graphsListAll[key]
        for key_bx in sorted(graphsList.iterkeys()):
            graphsList[key_bx].Write()

    rfile.Write()
    rfile.Close()

    with open(outputDir + outFileName + '.pkl', 'wb') as file:
        pickle.dump(graphsListAll, file)

    misseddata=open(outputDir+"makeGraphsFile_MissedData.log",'w')
    misseddata.write(missedDataBuffer)
    misseddata.close()

