#! /bin/bash

#ALL
#filearr=('6868_1806300950_1806301008.hd5' '6868_1806301056_1806301113.hd5' '6868_1806301122_1806301215.hd5' '6868_1806301303_1806301357.hd5' '6868_1806302208_1806302226.hd5' '6868_1806302221_1806302317.hd5' '6868_1806302313_1806302343.hd5' '6868_1807010054_1807010123.hd5' '6868_1807010145_1807010233.hd5' '6868_1807010545_1807010634.hd5' '6868_1807010645_1807010703.hd5' '6868_1807010700_1807010721.hd5');

#LONG + OFFSET
#filearr=('6868_1806301122_1806301215.hd5' '6868_1806301303_1806301357.hd5' '6868_1806302221_1806302317.hd5' '6868_1806302313_1806302343.hd5' '6868_1807010054_1807010123.hd5' '6868_1807010145_1807010233.hd5' '6868_1807010545_1807010634.hd5' '6868_1806301211_1806301306.hd5' '6868_1807010005_1807010058.hd5')

#Long Scans Only
filearr=('6868_1806301122_1806301215.hd5' '6868_1806301303_1806301357.hd5' '6868_1806302221_1806302317.hd5' '6868_1806302313_1806302343.hd5' '6868_1807010054_1807010123.hd5' '6868_1807010145_1807010233.hd5' '6868_1807010545_1807010634.hd5');

#Playground
#filearr=('6868_1806301122_1806301215.hd5')

#filearr=('6847_1806261036_1806261055.hd5' '6847_1806261055_1806261130.hd5' '6847_1806261127_1806261151.hd5');


#:<<'JK'

comm02='python AutoAnalysis.py -t -bgf -b -f /brildata/vdmdata18/'

for file02 in ${filearr[@]}
do
        if [ ${file02} == "6868_1806302313_1806302343.hd5" ];
        then
                eval "python AutoAnalysis.py -t -bgf -b -f /brildata/vdmdata18/6868_1806302313_1806302343.hd5 -f2 /brildata/vdmdata18/6868_1806302339_1807010009.hd5"


        elif [ $file02 == '6868_1807010054_1807010123.hd5' ];
        then
                eval "python AutoAnalysis.py -t -bgf -b -f /brildata/vdmdata18/6868_1807010054_1807010123.hd5 -f2 /brildata/vdmdata18/6868_1807010120_1807010149.hd5"

        else
                eval "${comm02}${file02}"
        fi
done

#JK

:<<'NC'

comm02='python AutoAnalysis.py -t -f /brildata/vdmdata18/'

for file02 in ${filearr[@]}
do
        if [ ${file02} == "6868_1806302313_1806302343.hd5" ];
        then
                eval "python AutoAnalysis.py -t -f /brildata/vdmdata18/6868_1806302313_1806302343.hd5 -f2 /brildata/vdmdata18/6868_1806302339_1807010009.hd5"


        elif [ $file02 == '6868_1807010054_1807010123.hd5' ];
        then
                eval "python AutoAnalysis.py -t -f /brildata/vdmdata18/6868_1807010054_1807010123.hd5 -f2 /brildata/vdmdata18/6868_1807010120_1807010149.hd5"

        else
                eval "${comm02}${file02}"
        fi
done

NC

:<<'A'

comm02='python AutoAnalysis.py -t -pdf -bgf -od -b -f /brildata/vdmdata18/'

for file02 in ${filearr[@]}
do
        if [ ${file02} == "6868_1806302313_1806302343.hd5" ];
        then
                eval "python AutoAnalysis.py -t -pdf -bgf -od -b -f /brildata/vdmdata18/6868_1806302313_1806302343.hd5 -f2 /brildata/vdmdata18/6868_1806302339_1807010009.hd5"


        elif [ $file02 == '6868_1807010054_1807010123.hd5' ];
        then
                eval "python AutoAnalysis.py -t -pdf -bgf -od -b -f /brildata/vdmdata18/6868_1807010054_1807010123.hd5 -f2 /brildata/vdmdata18/6868_1807010120_1807010149.hd5"

        else
                eval "${comm02}${file02}"
        fi
done


comm02='python AutoAnalysis.py -t -pdf -bgf -od -b -db -f /brildata/vdmdata18/'

for file02 in ${filearr[@]}
do
        if [ ${file02} == "6868_1806302313_1806302343.hd5" ];
        then
                eval "python AutoAnalysis.py -t -pdf -bgf -od -b -db -f /brildata/vdmdata18/6868_1806302313_1806302343.hd5 -f2 /brildata/vdmdata18/6868_1806302339_1807010009.hd5"


        elif [ $file02 == '6868_1807010054_1807010123.hd5' ];
        then
                eval "python AutoAnalysis.py -t -pdf -bgf -od -b -db -f /brildata/vdmdata18/6868_1807010054_1807010123.hd5 -f2 /brildata/vdmdata18/6868_1807010120_1807010149.hd5"

        else
                eval "${comm02}${file02}"
        fi
done

A

:<<'END'

comm02='python AutoAnalysis.py -t -bgf -od -b -db -ls -pdf -f /brildata/vdmdata18/'

for file02 in ${filearr[@]}
do
        if [ ${file02} == "6868_1806302313_1806302343.hd5" ];
        then
                eval "python AutoAnalysis.py -t -bgf -od -b -db -ls -pdf -f /brildata/vdmdata18/6868_1806302313_1806302343.hd5 -f2 /brildata/vdmdata18/6868_1806302339_1807010009.hd5"


        elif [ $file02 == '6868_1807010054_1807010123.hd5' ];
        then
                eval "python AutoAnalysis.py -t -bgf -od -b -db -ls -pdf -f /brildata/vdmdata18/6868_1807010054_1807010123.hd5 -f2 /brildata/vdmdata18/6868_1807010120_1807010149.hd5"

        else
                eval "${comm02}${file02}"
        fi
done

END
