import ROOT as r

import tables
import numpy as np
from scipy import stats
import math
import pandas as pd

import json
import os
import datetime as dt


def getRates(datapath, rateTable, scanpt, fill, fom, fomdata):
    '''Gets the data in the corresponding folder or hd5 file for the respective ratetable and scan point'''
    # print "beginning of getCurrents", scanpt

    bxdata = []
    avgdata = []
    bxdf = pd.DataFrame()
    avgdf = pd.DataFrame()

    rates = {}
    ratesErr = {}
    collBunches=[]

    # omit very first nibble because it may not be fully contained in VdM scan
    tw = '(timestampsec >' + str(scanpt[0]) + ') & (timestampsec <=' +  str(scanpt[1]) + ')'
    if datapath[-4:] == '.hd5':
        filelist = [datapath]
    else:        
        filelist = [f for f in os.listdir(datapath)]
    for f in filelist:
        if f!=datapath and (not str.isdigit(f[0]) or f[:4])!=fill:
            continue
        if f!=datapath:
            f = datapath + '/' + f
        beamtable = None
        with tables.open_file(f, 'r') as h5file:
            for table in h5file.root:
                if table.name == rateTable:
                    beamtable = table
                    break
            if not beamtable:
                print 'No table name ' + rateTable + ' in this file - ' + f
                #return bxdata, avgdata, bxdf, avgdf, rates, ratesErr, collBunches
                continue

                
            # sum over all bx
            #--------------------------------------------------------------------------------------------------------------------
            if fom==True:
	        with open(fomdata) as FD:
		    fomdict = json.load(FD)
		
		frev=11245.5                 

                if rateTable in fomdict.keys():
                    c1T=frev/fomdict[rateTable]["train"]
		    c1L=frev/fomdict[rateTable]["lead"]
		    slopeT=fomdict[rateTable]["slopeTrain"][0]
		    slopeL=fomdict[rateTable]["slopeLead"][0]
		    if fomdict[rateTable]["slopeSwitchFill"] != 0:
			for c, fl in enumerate(fomdict[rateTable]["slopeSwitchFill"]):
                                EorL=c if fill < fl else c+1
		    	slopeT=fomdict[rateTable]["slopeTrain"][EorL]
		    	slopeL=fomdict[rateTable]["slopeLead"][EorL]
                    c2T=-c1T*c1T*slopeT
                    c2L=-c1L*c1L*slopeL
            
	    #--------------------------------------------------------------------------------------------------------------------
            if rateTable != 'pltslinklumi':
                table = [(r['avgraw'], r['bxraw']) for r in beamtable.where(tw)]
                avglist = [r[0] for r in table]
                if avglist:
                    avgdata = avgdata + avglist
                # rates per bx 
                bxlist = [r[1] for r in table]
	    #--------------------------------------------------------------------------------------------------------------------
		if fom==True:
		    for c, row in enumerate(bxlist):
			for i in range(0,3564):
			    #print "first", bxlist[c][i]
			    if i == 0:
				bxlist[c][i]=c1L*bxlist[c][i]+c2L*bxlist[c][i]*bxlist[c][i]
			    if bxlist[c][i-1] < 0.1*bxlist[c][i]:
				bxlist[c][i]=c1L*bxlist[c][i]+c2L*bxlist[c][i]*bxlist[c][i]
			    else:
				bxlist[c][i]=c1T*bxlist[c][i]+c2T*bxlist[c][i]*bxlist[c][i]
			    #print "second", bxlist[c][i]
            #---------------------------------------------------------------------------------------------------------------------------
 
            else:
                avglist = [r['avgraw'] for r in beamtable.where(tw)]
                
                if avglist:
                    avgdata = avgdata + avglist
                # rates per bx 
                bxlist = [[r for i in range(3564)] for r in avglist]
            
	    if bxlist:
                bxdata = bxdata + bxlist
            if int(fill) >= 7427 and int(fill)<=7493:
		removestrays = lambda a: np.array([0 if i < 6e8 else 1 for i in a])
	    else:
	        removestrays = lambda a: np.array([0 if i < 6e9 else 1 for i in a])
            table = [(removestrays(r['bxintensity1']), removestrays(r['bxintensity2'])) for r in h5file.root.beam.where(tw)]
            bunchlist1 = [r[0] for r in table] 
            bunchlist2 = [r[1] for r in table] 

            if bunchlist1 and bunchlist2:
                collBunches  = np.nonzero(bunchlist1[0]*bunchlist2[0])[0].tolist()

    bxdf = pd.DataFrame(bxdata)
    avgdf = pd.DataFrame(avgdata)

    #rates for colliding bunches
    if bxdf.empty:
        print "Attention, rates bxdf empty because timestamp window not contained in file"
    else:
        for idx, bcid in enumerate(collBunches):
            i = bcid if int(fill)>=5838 or rateTable != 'hfetlumi' else (bcid-1 if bcid!=0 else 3563)
            rates[str(bcid+1)] = bxdf[i].mean()
            #or Fill == '6016' added by YusufCan
	    if rateTable == 'hfetlumi' or fill == '6016':
                ratesErr[str(bcid+1)] = stats.sem(bxdf[i])
            else:
		try:
                    ratesErr[str(bcid+1)] = math.sqrt(bxdf[i].mean()*(1-bxdf[i].mean())/(4*4096*len(bxdf[i])))
		except ValueError:
		    #print "In Error is", (1-bxdf[i].mean())
		    ratesErr[str(bcid+1)] = 0
		    if fom:
			ratesErr[str(bcid+1)] = bxdf[i].std()
    if avgdf.empty:
        print "Attention, rates avgdf empty because timestamp window not contained in file"
    else:
        rates['sum'] = avgdf[0].mean()
        ratesErr['sum'] = stats.sem(avgdf[0])

    return (rates, ratesErr)

def doMakeRateFile(ConfigInfo):
    
    AnalysisDir = str(ConfigInfo['AnalysisDir'])
    InputScanFile = AnalysisDir + "/" + str(ConfigInfo['InputScanFile'])
    InputLumiDir = str(ConfigInfo['InputLumiDir'])
    RateTable = str(ConfigInfo['RateTable'])
    FOMData = str(ConfigInfo['FOM_Data'])
    FOM = ConfigInfo['FOM']

    with open(InputScanFile, 'rb') as f:
        scanInfo = json.load(f)

    table = {}
    for i, name in enumerate(scanInfo["ScanNames"]):
        key = "Scan_" + str(i+1)
        scanpoints = scanInfo[key]
        table[key]=[]
        for j, sp in enumerate(scanpoints):
            # print "Scan point", j, sp
            rates = getRates(InputLumiDir, RateTable, sp[3:],scanInfo["Fill"], FOM, FOMData)
            scanpoint = {
                'ScanNumber':i+1,
                'ScanName':name,
                'ScanPoint':j+1,
                'Rates':rates[0],
                'RateErrs':rates[1]}
            table[key].append(scanpoint)

    return table


if __name__ == '__main__':

    import sys, json, pickle

    # read config file
    ConfigFile = sys.argv[1]

    Config=open(ConfigFile)
    ConfigInfo = json.load(Config)
    Config.close()

    Luminometer = str(ConfigInfo['Luminometer'])
    AnalysisDir = str(ConfigInfo['AnalysisDir'])
    OutputSubDir = AnalysisDir + "/" + str(ConfigInfo['OutputSubDir'])

    InputScanFile = './' + AnalysisDir + '/' + str(ConfigInfo['InputScanFile'])
    with open(InputScanFile, 'rb') as f:
        scanInfo = pickle.load(f)

    Fill = scanInfo["Fill"]     

    table = {}
    csvtable = []

    table = doMakeRateFile(ConfigInfo)

    with open(OutputSubDir+'/Rates_'+str(Luminometer)+'_'+str(Fill)+'.json', 'wb') as f:
        json.dump(table, f)
            





