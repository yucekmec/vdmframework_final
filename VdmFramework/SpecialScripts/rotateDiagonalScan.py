import tables as t, pylab as py

folder =  '/brildata/vdmdata18/'
fin = '7442_1811132148_1811132229.hd5'
#fin = '7443_1811141822_1811141858_fixed.hd5'
fin = '7443_1811142106_1811142134.hd5'

fout = fin.split('.')[0]+'_fixed.hd5'


compr_filter = t.Filters(complevel=9, complib='blosc')
chunkshape=(100,)



h5in = t.open_file(folder+fin)
h5out = t.open_file(folder+fout,mode='w')


nsp = h5in.root.vdmscan[:]['nominal_sep_plane']
nsp[py.where(nsp[0:len(nsp)/2+2]=='CROSSING')] = 'SEPARATION'
sep = h5in.root.vdmscan[:]['sep']*2**0.5

for table in h5in.iter_nodes('/'):
	# if table.name != 'vdmscan':
	descr = table.description._v_colobjects.copy()
	outtable = h5out.create_table('/',table.name,descr,filters=compr_filter,chunkshape=chunkshape)
	table.append_where(outtable,condition ='timestampsec > 0')

i=0
for row in h5out.root.vdmscan.iterrows():
#	print row['lsnum'], row['nominal_sep_plane'],nap
	row['nominal_sep_plane'] = nsp[i]
	row['sep'] = sep[i]
	row.update()
	i +=1

h5out.root.vdmscan.flush()



h5in.close()
h5out.close()


