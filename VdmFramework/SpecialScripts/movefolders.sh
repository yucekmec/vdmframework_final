#! /bin/bash

#filearr=('6868_1806300950_1806301008.hd5' '6868_1806301056_1806301113.hd5' '6868_1806301122_1806301215.hd5' '6868_1806301211_1806301306.hd5' '6868_1806301303_1806301357.hd5' '6868_1806302208_1806302226.hd5' '6868_1806302221_1806302317.hd5' '6868_1806302313_1806302343.hd5' '6868_1807010005_1807010058.hd5' '6868_1807010054_1807010123.hd5' '6868_1807010145_1807010233.hd5' '6868_1807010545_1807010634.hd5' '6868_1807010645_1807010703.hd5' '6868_1807010700_1807010721.hd5');

filearr=('PLT' 'HFOC' 'HFET' 'BCM1FPCVD' 'BCM1FSI' 'BCM1FUTCA_PCVD_OLD' 'BCM1FUTCA_PCVD_DERIV' 'BCM1FUTCA_DERIV1' 'BCM1FUTCA_DERIV2' 'BCM1FUTCA_DERIV9' 'BCM1FUTCA_DERIV10' 'BCM1FUTCA_DERIV16' 'BCM1FUTCA_DERIV17' 'BCM1FUTCA_DERIV18' 'BCM1FUTCA_DERIV33' 'BCM1FUTCA_DERIV41' 'BCM1FUTCA_DERIV42' 'BCM1FUTCA_OLD1' 'BCM1FUTCA_OLD2' 'BCM1FUTCA_OLD9' 'BCM1FUTCA_OLD10' 'BCM1FUTCA_OLD16' 'BCM1FUTCA_OLD17' 'BCM1FUTCA_OLD18' 'BCM1FUTCA_OLD33' 'BCM1FUTCA_OLD41' 'BCM1FUTCA_OLD42');
#filearr=('6868_1806302313_1806302343.hd5' '6868_1807010054_1807010123.hd5');

#filearr=('6868_1806302313_1806302343.hd5');

#filearr=('6868_1807010005_1807010058.hd5' '6868_1806301211_1806301306.hd5');
#filearr=('6868_1806301211_1806301306.hd5');

#:<<'END'
comm1='cp -r ./6868_01Jul18_021315_01Jul18_023437/'
comm2='/results/BeamBeam/ ./Automation/Analysed_Data/6868_01Jul18_021315_01Jul18_023437/'
comm3='/results/'
comm4='cp -r ./6868_30Jun18_141812_30Jun18_144307/'
comm5='/results/BeamBeam/ ./Automation/Analysed_Data/6868_30Jun18_141812_30Jun18_144307/'
for file in ${filearr[@]}
do
        eval "${comm1}${file}${comm2}${file}${comm3}"
	eval "${comm4}${file}${comm5}${file}${comm3}"
done

:<<'END'

comm1='python AutoAnalysis.py  -d -bg -pdf -f /brildata/vdmdata18/'

for file1 in ${filearr[@]}
do
        if [ ${file1} == "6868_1806302313_1806302343.hd5" ];
        then
                eval "python AutoAnalysis.py -d -bg -pdf -f /brildata/vdmdata18/6868_1806302313_1806302343.hd5 -f2 /brildata/vdmdata18/6868_1806302339_1807010009.hd5"


        elif [ $file1 == '6868_1807010054_1807010123.hd5' ];
        then
                eval "python AutoAnalysis.py -d -bg -pdf -f /brildata/vdmdata18/6868_1807010054_1807010123.hd5 -f2 /brildata/vdmdata18/6868_1807010120_1807010149.hd5"

        else
                eval "${comm1}${file1}"
        fi
done


#BG


comm2='python AutoAnalysis.py  -d -b -bg -pdf -f /brildata/vdmdata18/'

for file2 in ${filearr[@]}
do
        if [ ${file2} == "6868_1806302313_1806302343.hd5" ];
        then
                eval "python AutoAnalysis.py -d -b -bg -pdf -f /brildata/vdmdata18/6868_1806302313_1806302343.hd5 -f2 /brildata/vdmdata18/6868_1806302339_1807010009.hd5"


        elif [ $file2 == '6868_1807010054_1807010123.hd5' ];
        then
                eval "python AutoAnalysis.py -d -b -bg -pdf -f /brildata/vdmdata18/6868_1807010054_1807010123.hd5 -f2 /brildata/vdmdata18/6868_1807010120_1807010149.hd5"

        else
                eval "${comm2}${file2}"
        fi
done



comm3='python AutoAnalysis.py -d -b -bg -ls -pdf -f /brildata/vdmdata18/'

for file3 in ${filearr[@]}
do
        if [ ${file3} == "6868_1806302313_1806302343.hd5" ];
        then
                eval "python AutoAnalysis.py -d -b -bg -ls -pdf -f /brildata/vdmdata18/6868_1806302313_1806302343.hd5 -f2 /brildata/vdmdata18/6868_1806302339_1807010009.hd5"


        elif [ $file3 == '6868_1807010054_1807010123.hd5' ];
        then
                eval "python AutoAnalysis.py -d -b -bg -ls -pdf -f /brildata/vdmdata18/6868_1807010054_1807010123.hd5 -f2 /brildata/vdmdata18/6868_1807010120_1807010149.hd5"

        else
                eval "${comm3}${file3}"
        fi
done

comm02='python AutoAnalysis.py -b -pdf -f /brildata/vdmdata18/'

for file02 in ${filearr[@]}
do
        if [ ${file02} == "6868_1806302313_1806302343.hd5" ];
        then
                eval "python AutoAnalysis.py -b -pdf -f /brildata/vdmdata18/6868_1806302313_1806302343.hd5 -f2 /brildata/vdmdata18/6868_1806302339_1807010009.hd5"


        elif [ $file02 == '6868_1807010054_1807010123.hd5' ];
        then
                eval "python AutoAnalysis.py -b -pdf -f /brildata/vdmdata18/6868_1807010054_1807010123.hd5 -f2 /brildata/vdmdata18/6868_1807010120_1807010149.hd5"

        else
                eval "${comm02}${file02}"
        fi
done

#:<<'BG2'

comm12='python AutoAnalysis.py -bg -pdf -f /brildata/vdmdata18/'

for file12 in ${filearr[@]}
do
        if [ ${file12} == "6868_1806302313_1806302343.hd5" ];
        then
                eval "python AutoAnalysis.py -bg -pdf -f /brildata/vdmdata18/6868_1806302313_1806302343.hd5 -f2 /brildata/vdmdata18/6868_1806302339_1807010009.hd5"


        elif [ $file12 == '6868_1807010054_1807010123.hd5' ];
        then
                eval "python AutoAnalysis.py -bg -pdf -f /brildata/vdmdata18/6868_1807010054_1807010123.hd5 -f2 /brildata/vdmdata18/6868_1807010120_1807010149.hd5"

        else
                eval "${comm12}${file12}"
        fi
done


#BG2



comm22='python AutoAnalysis.py -b -bg -pdf -f /brildata/vdmdata18/'

for file22 in ${filearr[@]}
do
        if [ ${file22} == "6868_1806302313_1806302343.hd5" ];
        then
                eval "python AutoAnalysis.py -b -bg -pdf -f /brildata/vdmdata18/6868_1806302313_1806302343.hd5 -f2 /brildata/vdmdata18/6868_1806302339_1807010009.hd5"


        elif [ $file22 == '6868_1807010054_1807010123.hd5' ];
        then
                eval "python AutoAnalysis.py -b -bg -pdf -f /brildata/vdmdata18/6868_1807010054_1807010123.hd5 -f2 /brildata/vdmdata18/6868_1807010120_1807010149.hd5"

        else
                eval "${comm22}${file22}"
        fi
done



comm32='python AutoAnalysis.py -b -bg -ls -pdf -f /brildata/vdmdata18/'

for file32 in ${filearr[@]}
do
        if [ ${file32} == "6868_1806302313_1806302343.hd5" ];
        then
                eval "python AutoAnalysis.py -b -bg -ls -pdf -f /brildata/vdmdata18/6868_1806302313_1806302343.hd5 -f2 /brildata/vdmdata18/6868_1806302339_1807010009.hd5"


        elif [ $file32 == '6868_1807010054_1807010123.hd5' ];
        then
                eval "python AutoAnalysis.py -b -bg -ls -pdf -f /brildata/vdmdata18/6868_1807010054_1807010123.hd5 -f2 /brildata/vdmdata18/6868_1807010120_1807010149.hd5"

        else
                eval "${comm32}${file32}"
        fi
done
END
