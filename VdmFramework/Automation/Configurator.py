import argparse
import datetime as dt
import json
import logging
import os
import traceback
import fnmatch

import numpy as np
import pandas as pd

luminometers = ['PLT', 'HFLumi', 'BCM1F', 'HFLumiET']
fits = ['SG', 'SGConst', 'SGConst', 'SGConst']
ratetables = ['pltlumizero', 'hflumi', 'bcm1flumi', 'hfetlumi']


def GetTimestamps(data, fillnum, automation_folder='Automation/', muscan=False):
    """Gets the VdM scan start and end rows from a DIP 
        VdM dataframe. Saves a new reduced vdm file as automation_folder + 'dipfiles/vdm_'
        + name + '.csv'. 

        data : the data from a vdm dip csv file or vdmscan table of hd5 files remapped by RemapVdMDIPData
        name : the name of the analysed data folder (fill_datestart_timestart_dateend_timend usually)
        automation_folder : the relative path to folder with your dipfiles, autoconfigs and Analysed_Data folders

        Returns:
        timestamp_data : An array of pairs like 
            [ (timestamp, nominal_separation_plane), (timestamp, nominal_separation_plane) ] 
            (beginning and end of scan respectively)
        step_data : the number of steps in each scan (ordered)
     """
    # Get cms data for fill. 
    # NOTE: IP should be converted to binary and it's 5th bit checked,
    # since if a scan was happening at both p1 and p5 we would get 34, e.g.
    data = data[(data.ip == 32) & (data.fill == fillnum) & (data.scanstatus == 'ACQUIRING')]
    if muscan==False:
    	if 'nominal_separation_plane' in data:
    	    data = data[(data.nominal_separation_plane != 'NONE')]

    #if 'plane' in data:
    #    data=data[(data.plane!='MIXED')]

    if data.shape[0] == 0:
        message = '\n\t' + 'No data for fill ' + str(fillnum)
        print(message)
        logging.warning('\n\t' + dt.datetime.now().strftime(
            '%d %b %Y %H:%M:%S\n') + message)
        raise Exception('No times')

    # Get start and end rows
                        # (data.nominal_separation_plane != data.shift(1).nominal_separation_plane) |
                        #    (data.nominal_separation_plane != data.shift(-1).nominal_separation_plane) |
                           
    timestamp_data = data[((data.plane != data.shift(-1).plane) | (data.plane != data.shift(1).plane) |
                        (data.nominal_separation_plane != data.shift(1).nominal_separation_plane) |
                        (data.nominal_separation_plane != data.shift(-1).nominal_separation_plane) |
                        (data.step - data.shift(1).step < 0) | (data.shift(-1).step - data.step < 0))]  # .loc[:,['fill','sec','plane','nominal_separation_plane']]
    
    if muscan==True:
	timestamp_data = data[((data.plane != data.shift(-1).plane) | (data.plane != data.shift(1).plane) |
                             (data.step - data.shift(1).step < 0) | (data.shift(-1).step - data.step < 0))]
                       
    print(timestamp_data.loc[:, ['fill', 'sec', 'plane', 'nominal_separation_plane']])
    logging.debug('Timestamps (that look like beginnings and endings of scans):\n' + 
                 str(timestamp_data.loc[:, ['fill', 'sec', 'plane', 'nominal_separation_plane']]))

    step_data = []
    # Remove rows with timestamps which don't look like scans and sort the
    # rest into pairs (each pair is a scan)
    for t1, t2 in zip(timestamp_data.index[::2], timestamp_data.index[1::2]):

	if muscan==True:
	    break

        # Something happened during VdM that made this think there were multiple super small scans, so I added this
        if t2 - t1 < 30:
            timestamp_data = timestamp_data[(
                timestamp_data.index != t1) & (timestamp_data.index != t2)]
            message = '\n\t' + 'Timestamps ' + str(data.get_value(t1, 'sec')) + ' and ' + str(data.get_value(
                t2, 'sec')) + ' removed due to scan under 30 seconds'
            print(message)
            logging.warning('\n\t' + dt.datetime.now().strftime(
                '%d %b %Y %H:%M:%S\n') + message)
            continue
        ## Not really a dealbreaker I guess
        # Scan should finish progress, and it goes down from a certain max
        # number to 1, we don't get data for each but we should always get
        # at least last 3
        # if data.get_value(t2, 'progress') > 3:
        #     timestamp_data = timestamp_data[(
        #         timestamp_data.index != t1) & (timestamp_data.index != t2)]
        #     message = '\n\t' + 'Timestamps ' + str(data.get_value(t1, 'sec')) + ' and ' + str(data.get_value(
        #         t2, 'sec')) + ' removed due to unfinished step'
        #     print(message)
        #     logging.warning('\n\t' + dt.datetime.now().strftime(
        #         '%d %b %Y %H:%M:%S\n') + message)
        #     continue


        # step number 13 is the proper number for a 7 step emittance scan.
        # filters out some completely bogged up scans
        # These are now mostly 9 step so maybe it should be changed
        if data.get_value(t2, 'step') < 13:
            timestamp_data = timestamp_data[(
                timestamp_data.index != t1) & (timestamp_data.index != t2)]
            message = '\n\t' + 'Timestamps ' + str(data.get_value(t1, 'sec')) + ' and ' + str(data.get_value(
                t2, 'sec')) + ' removed due to last step number less then 13'
            print(message)
            logging.warning('\n\t' + dt.datetime.now().strftime(
                '%d %b %Y %H:%M:%S\n') + message)
            continue
        reduced_data = data.loc[t1:t2, ['nominal_separation', 'sec']]
        nom_seps = reduced_data[reduced_data.nominal_separation !=
                            reduced_data.shift(-1).nominal_separation]
        logging.debug(nom_seps)

        # Constant nominal separation is not a scan
        if nom_seps.empty:
            message = '\n\t' + 'Timestamps ' + str(data.get_value(t1, 'sec')) + ' and ' + str(data.get_value(
                t2, 'sec')) + ' removed due to constant nominal separation'
            print(message)
            logging.warning('\n\t' + dt.datetime.now().strftime(
                '%d %b %Y %H:%M:%S\n') + message)
            timestamp_data = timestamp_data[(
                timestamp_data.index != t1) & (timestamp_data.index != t2)]
            continue

        # A scan's nominal separation in time should be a monotonous
        # function: every step makes it larger(smaller) as it goes from -A
        # (+A) to +A (-A)
        diffsigns = np.sign(nom_seps.nominal_separation -
                            nom_seps.shift(1).nominal_separation)
        for i in range(len(diffsigns))[2:]:
            if (diffsigns.iloc[i] != diffsigns.iloc[i - 1]):
                # Remove the last step if it was back to top
                if (i == (len(diffsigns) - 1)) & (nom_seps.iloc[i].nominal_separation < 0.0001):
                    # We only really care about the seconds and the plane,
                    # which should be the same but I need to keep the index
                    # up to this point, so no reason to cut the other columns
                    timestamp_data.set_value(t2, 'sec', nom_seps.sec.tolist()[-2])
                    message = '\n\t' + 'Removed last step of scan beginning at ' + \
                              str(data.get_value(t1, 'sec')) + ' because it is flattop'
                    print(message)
                    logging.warning('\n\t' + dt.datetime.now().strftime(
                                    '%d %b %Y %H:%M:%S\n') + message)
                    continue
                message = '\n\t' + 'Timestamps ' + str(data.get_value(t1, 'sec')) + ' and ' + str(data.get_value(
                    t2, 'sec')) + ' removed due to non monotonous nominal separation in time'
                print(message)
                logging.warning('\n\t' + dt.datetime.now().strftime(
                    '%d %b %Y %H:%M:%S\n') + message)
                timestamp_data = timestamp_data[(
                    timestamp_data.index != t1) & (timestamp_data.index != t2)]
                break
        step_data.append(data.get_value(t2, 'step')/2)
    if 'nominal_separation_plane' in timestamp_data:
        timestamp_data = map(lambda c, d: (c, d),
                         timestamp_data.sec, timestamp_data.nominal_separation_plane)
    else:
        timestamp_data = map(lambda c,d: (c,'CROSSING' if d == 0 else 'SEPARATION'), timestamp_data.sec, timestamp_data.set_nominal_B1sepPlane)
    timestamp_data = map(lambda c, d: [c, d],
                         timestamp_data[::2], timestamp_data[1::2])


    # Remove last scan if beam was dumped before producing second part of
    # scanpair
    # if len(timestamp_data) % 2 == 1:
    #     message = '\n\t' + 'Uneven number of scans, removing last scan: ' + \
    #         str(timestamp_data[-1])
    #     timestamp_data = timestamp_data[:-1]
    #     print message
    #     logging.warning('\n\t' + dt.datetime.now().strftime(
    #         '%d %b %Y %H:%M:%S\n') + message)
    print('Timestamps for start and end of scans:')
    print(timestamp_data)
    #print len(timestamp_data)
    return timestamp_data, step_data

##############################################
def FormatTimestamps(times):
    """Formats the timestamps into corresponding scan names, timewindows,
        scan pairs, offsets and removes nominal separation plane data
        
        times are in the format that comes out of GetTimestamps
        """
    # Create scan names and pairs
    if not times:
        return False
    rang = range(1, len(times) + 1)
    pairs = []#map(lambda a, b: [a, b], rang[::2], rang[1::2])
    names = []
    crossfirst = True
    for i in xrange(0, len(times)/2):
        if times[2 * i][0][1] == 'CROSSING':
            names.append("X" + str(i + 1))
            names.append("Y" + str(i + 1))
            crossfirst = True
        else:
            names.append("Y" + str(i + 1))
            names.append("X" + str(i + 1))
            crossfirst = False
        if i%2 == 0:
            pairs.append([i+1,i+2] if crossfirst else [i+2,i+1])

    # Get scan data in correct format for auto config
    times = map(lambda a: [a[0][0], a[1][0]], times)

    _scannames = json.dumps(names)
    _timewindows = str(times)
    _scanpairs = str(pairs)
    #_offsets = str([0.0 for i in range(len(times))])
    #_offsets = str([0.29526 for i in range(len(times))])

    #return _scannames, _timewindows, _scanpairs, _offsets, times
    return _scannames, _timewindows, _scanpairs, times

##############################################
def GetOffsets(times,dipfile):

    print times

    df = pd.read_csv(dipfile)
    extractedList = df.columns.values.tolist()

    #print extractedList

    offsets = [0.0 for i in range(len(times))]

    # element in times coresponds to one scan
    for i in range(len(times)):
        start=times[i][0]
        finish=times[i][1]

        #df.index=df['sec']

        #print df.index

        offsetdata=df[(df['sec']>=start)&(df['sec']<=finish)][['set_nominal_B1sepPlane','set_nominal_B1xingPlane','set_nominal_B2sepPlane','set_nominal_B2xingPlane','nominal_separation_plane']]

        if offsetdata.iloc[0]['nominal_separation_plane']=='CROSSING':

            serie=offsetdata['set_nominal_B1sepPlane'].tolist()
            corrtest1=serie.count(serie[0])
            serie=offsetdata['set_nominal_B2sepPlane'].tolist()
            corrtest2=serie.count(serie[0])

            if (corrtest1!=len(offsetdata))|(corrtest2!=len(offsetdata)):
                print "Attention from GetOffsets: offset is not constant in scan ", i, ", check dip file, mean value is used"

            offset=offsetdata['set_nominal_B1sepPlane']-offsetdata['set_nominal_B2sepPlane']
            offsets[i]=offset.mean()

        elif offsetdata.iloc[0]['nominal_separation_plane']=='SEPARATION':

            serie=offsetdata['set_nominal_B1xingPlane'].tolist()
            corrtest1=serie.count(serie[0])
            serie=offsetdata['set_nominal_B2xingPlane'].tolist()
            corrtest2=serie.count(serie[0])

            if (corrtest1!=len(offsetdata))|(corrtest2!=len(offsetdata)):
                print "Attention from GetOffsets: offset is not constant in scan ", i, ", check dip file, mean value is used"

            offset=offsetdata['set_nominal_B1xingPlane']-offsetdata['set_nominal_B2xingPlane']
            offsets[i]=offset.mean()

        else:
            print "Something wrong with nominal_separation_plane", offsetdata.iloc[0]['nominal_separation_plane']
            print "Attention from GetOffsets: can not get offset data, use zero values"
            offsets[i] = 0.0

    #_offsets = str([0.0 for i in range(len(times))])
    #_offsets = str([0.29526 for i in range(len(times))])

    print "Offsets=", offsets

    return str(offsets)

##############################################
def ConfigDriver(times, fillnum, _luminometer, _fit, _ratetable, name, central, filename1, filename2, fixedbackground, muscan, fom, BeamEnergy, corr = ['noCorr'], automation_folder='Automation/', _bstar = False, _angle = False, makepdf = True, makelogs = True, autoconfigs_folder = 'na', dip=False):
    """Makes a folder with configuration files with given timestamps paired for beginnings and endings of scans

        times : should be of the form [ (timestamp, nominal_separation_plane), (timestamp, nominal_separation_plane) ] (like from GetTimestamps)
        name : the name of the analysis folder (fill number and datetimes from the beginning and ending of the scan pair)
        central : the path to the data file or folder. give hd5 file path for emittance scans and folder (currently /brildata/vdmdata17/)
        automation_folder : the relative path to folder with your dipfiles, autoconfigs and Analysed_Data folders (default is 'Automation/')
        _bstar and _angle : should be values of those variables if you have them (otherwise default is False)
        makepdf : tells whether to make pdfs with beam beam corrections and fitted functions
        makelogs : tells whether to make logs for the fitting (minuit and otherwise)
        autoconfigs_folder : is the folder where your templates for configurations are"""

    # json-compatible true-false`
    true = json.dumps(True)
    false = json.dumps(False)

    if autoconfigs_folder == 'na':
        autoconfigs_folder = automation_folder

    _frameworkdir=os.getcwd()
    
    # time-related data for all configurations
    #_scannames, _timewindows, _scanpairs, _offsets, times = FormatTimestamps(times)
    _scannames, _timewindows, _scanpairs, times = FormatTimestamps(times)

    # other, more generic data
    _fill = str(fillnum)
    _date = dt.datetime.fromtimestamp(times[0][0]).strftime('%d%b%Y')
    _central = central  # + _fill
    _dip = dip if dip else automation_folder + '/dipfiles/dip_' + name + '.csv'

    _beamenergy = BeamEnergy   

    _offsets=GetOffsets(times,_dip)
    #print _offsets
	
    #MuScan Flag
    #_muscan = true if muscan else false
    _muscan = json.dumps(muscan)
    _fom = json.dumps(fom)

    #Background Config Items
    _fixedbackground = json.dumps(fixedbackground)
    _filename1 = filename1
    _filename2 = filename2

    _makeScanFile = false
    _makeRateFile = false
    _makeBeamCurrentFile = false
    _makeBeamBeamFile = false
    _makeLengthScaleFile = false
    _makeOrbitDriftFile = false
    _makeBackgroundFile = false
    _makeDynamicBetaFile = false
    _makeGraphsFile = true
    _runVdmFitter = true
    _makepdf = json.dumps(makepdf)
    _makelogs = json.dumps(makelogs)

    # Start without correction
    if ('Background' in corr) and ('OrbitDrift' in corr):
	_cs = ['Background','OrbitDrift','Background_OrbitDrift']
    elif ('Background' in corr) and ('OrbitDrift' not in corr):
	_cs = ['Background']
    else:
	_cs = ['noCorr']
    
    #_corr = 'noCorr' if 'Background' not in corr else 'Background'
    #_corrs = json.dumps([_corr])

    path = automation_folder + 'autoconfigs/'
    try:
        os.mkdir(path + name)
    except OSError as e:
        shit = 'happens'
        #print('Folder ' + path + _fill + ' already exists')
        #logging.warning('Folder ' + path + _fill + ' already exists' + '\n'  + str(e))
     
    def Config():
        '''Creates new Driver configuration
            Variables are set outside of function so you only change the ones you
            need different from the last configuration'''
	if ("Background" in _corr) and ("OrbitDrift" in _corr):
            _bbsource = 'Background_OrbitDrift'
        elif ("Background" in _corr) and ("OrbitDrift" not in _corr):
            _bbsource = 'Background'
        else:
            _bbsource = 'noCorr'

	'''
        if _bstar is False:
            with open(autoconfigs_folder + 'vdmDriverII_Autoconfig.json', 'r') as f:
                config = f.read()
	
            with open(path + name + '/' + _luminometer + _corr + '_' + _fit + '_driver.json', 'w') as f:
                f.write(config.format(fill=_fill, date=_date, scannames=_scannames, timewindows=_timewindows,
                                    analysisdir=automation_folder + 'Analysed_Data/' + name, scanpairs=_scanpairs, frameworkdir=_frameworkdir,
                                    dip=_dip, central=_central, filename1=_filename1, filename2=_filename2, fixed=_fixedbackground, muscan=_muscan, fom=_fom, luminometer=_luminometer, fit=_fit, offsets=_offsets,
                                    ratetable=_ratetable, corr=_corr, corrs=_corrs, makeScanFile=_makeScanFile,
                                    makeRateFile=_makeRateFile, makeBeamCurrentFile=_makeBeamCurrentFile, 
                                    makeBeamBeamFile=_makeBeamBeamFile, bbsource=_bbsource, beamenergy=_beamenergy,
                                    makeDynamicBetaFile=_makeDynamicBetaFile, makeLengthScaleFile=_makeLengthScaleFile, 
                                    makeBackgroundFile=_makeBackgroundFile, makeOrbitDriftFile=_makeOrbitDriftFile, makeGraphsFile=_makeGraphsFile,
                                    runVdmFitter=_runVdmFitter, makepdf = _makepdf, makelogs = _makelogs))
        else:
	'''
        with open(autoconfigs_folder + 'vdmDriverII_Autoconfig.json', 'r') as f:
            config = f.read()
        # print config
        # input()
        with open(path + name + '/' + _luminometer + _corr + '_' + _fit + '_driver.json', 'w') as f:
            f.write(config.format(fill=_fill, date=_date, scannames=_scannames, timewindows=_timewindows,
                                analysisdir=automation_folder + 'Analysed_Data/' + name, scanpairs=_scanpairs, frameworkdir=_frameworkdir,
                                dip=_dip, central=_central, filename1=_filename1, filename2=_filename2, fixed=_fixedbackground, muscan=_muscan, fom=_fom, luminometer=_luminometer, fit=_fit, offsets=_offsets,
                                ratetable=_ratetable, corr=_corr, corrs=_corrs, makeScanFile=_makeScanFile,
                                makeRateFile=_makeRateFile, makeBeamCurrentFile=_makeBeamCurrentFile, 
                                makeBeamBeamFile=_makeBeamBeamFile, bbsource=_bbsource, beamenergy=_beamenergy,
                                makeDynamicBetaFile=_makeDynamicBetaFile, makeLengthScaleFile=_makeLengthScaleFile, 
                                makeBackgroundFile=_makeBackgroundFile,makeGraphsFile=_makeGraphsFile, makeOrbitDriftFile=_makeOrbitDriftFile,
                                runVdmFitter=_runVdmFitter, bstar = _bstar, angle = _angle,
                                makepdf = _makepdf, makelogs = _makelogs))
        # Configure calibration constant calculation
        with open(autoconfigs_folder + 'calculateCalibrationConstant_Autoconfig.json', 'r') as f:
            config = f.read()

        with open(path + name + '/' + _luminometer + _corr +
                '_' + _fit + '_calibrationConst.json', 'w') as f:
            f.write(config.format(fill=_fill, date=_date, scanpairs=_scanpairs, analysisdir=automation_folder + 'Analysed_Data/' + name,
                                luminometer=_luminometer, fit=_fit, corrs=_corrs))

        # Configure fit results plotting
        with open(autoconfigs_folder + 'plotFitResults_Autoconfig.json', 'r') as f:
            config = f.read()

        with open(path + name + '/' + _luminometer +
                _corr + '_' + _fit + '_plotFit.json', 'w') as f:
            f.write(config.format(fill=_fill, date=_date,
                                luminometer=_luminometer, fit=_fit, corrs=_corrs,analysisdir=automation_folder + 'Analysed_Data/' + name))

    # Configure a noCorr or BackgroundCorr driver run, to use as source for a run with the same correction plus a BeamBeam and/or DynamicBeta correction
    for _c in _cs:
	_makeOrbitDriftFile = json.dumps('OrbitDrift' in _c)
        _makeBackgroundFile = json.dumps('Background' in _c)
	if _c == 'Background_OrbitDrift':
            _corrs = json.dumps(['Background','OrbitDrift'])
	else:
	    _corrs = json.dumps([_c])
	_corr = _c
        Config()
	_makeOrbitDriftFile = false
        _makeBackgroundFile = false

    # Configure a full corrected driver run
    # BeamBeam, DynamicBeta need an input of capsigma measurements to have run before running, potentially some other corrections might too
    
    if corr != ['noCorr']:
        _corr = reduce(lambda a,b: str(a) + '_' + str(b), corr)
        _corrs = json.dumps(corr)
        _makeBeamBeamFile = json.dumps('BeamBeam' in corr)
        _makeDynamicBetaFile=json.dumps('DynamicBeta' in corr)
        _makeLengthScaleFile = json.dumps('LengthScale' in corr)
        _makeOrbitDriftFile = json.dumps('OrbitDrift' in corr)
        _makeBackgroundFile = json.dumps('Background' in corr)
        _makeGraphsFile = true
        _runVdmFitter = true

        Config()

##############################
def RemapVdMDIPData(olddata):
    '''Remaps a pandas dataframe from the format in which it comes to HD5 files to
        the old vdmdip csv file with added new fields and 0 for total atlas luminosity'''

    data = pd.DataFrame()
    data['fill'] = olddata['fillnum']
    data['run'] = olddata['runnum']
    data['ls'] = olddata['lsnum']
    data['nb'] = olddata['nbnum']
    data['sec'] = olddata['timestampsec']
    data['msec'] = olddata['timestampmsec']
    data['acqflag'] = [int(i == 'ACQUIRING') for i in olddata['stat']]
    data['step'] = olddata['step']
    data['beam'] = olddata['beam']
    data['ip'] = olddata['ip']
    data['scanstatus'] = olddata['stat']
    data['plane'] = olddata['plane']
    data['progress'] = olddata['progress']
    data['nominal_separation'] = olddata['sep']
    
    data['read_nominal_B1sepPlane'] = olddata['r_sepP1']
    data['read_nominal_B1xingPlane'] = olddata['r_xingP1']
    data['read_nominal_B2sepPlane'] = olddata['r_sepP2']
    data['read_nominal_B2xingPlane'] = olddata['r_xingP2']
    data['set_nominal_B1sepPlane'] = olddata['s_sepP1']
    data['set_nominal_B1xingPlane'] = olddata['s_xingP1']
    data['set_nominal_B2sepPlane'] = olddata['s_sepP2']
    data['set_nominal_B2xingPlane'] = olddata['s_xingP2']
    data['atlas_totInst'] = [0 for i in data['fill']]
    
    if 'nominal_sep_plane' in olddata:
        data['nominal_separation_plane'] = olddata['nominal_sep_plane']
        data['bpm_5LDOROS_B1Names'] = olddata['5ldoros_b1names']
        data['bpm_5LDOROS_B1hPos'] = olddata['5ldoros_b1hpos']
        data['bpm_5LDOROS_B1vPos'] = olddata['5ldoros_b1vpos']
        data['bpm_5LDOROS_B1hErr'] = olddata['5ldoros_b1herr']
        data['bpm_5LDOROS_B1vErr'] = olddata['5ldoros_b1verr']
        data['bpm_5RDOROS_B1Names'] = olddata['5rdoros_b1names']
        data['bpm_5RDOROS_B1hPos'] = olddata['5rdoros_b1hpos']
        data['bpm_5RDOROS_B1vPos'] = olddata['5rdoros_b1vpos']
        data['bpm_5RDOROS_B1hErr'] = olddata['5rdoros_b1herr']
        data['bpm_5RDOROS_B1vErr'] = olddata['5rdoros_b1verr']
        data['bpm_5LDOROS_B2Names'] = olddata['5ldoros_b2names']
        data['bpm_5LDOROS_B2hPos'] = olddata['5ldoros_b2hpos']
        data['bpm_5LDOROS_B2vPos'] = olddata['5ldoros_b2vpos']
        data['bpm_5LDOROS_B2hErr'] = olddata['5ldoros_b2herr']
        data['bpm_5LDOROS_B2vErr'] = olddata['5ldoros_b2verr']
        data['bpm_5RDOROS_B2Names'] = olddata['5rdoros_b2names']
        data['bpm_5RDOROS_B2hPos'] = olddata['5rdoros_b2hpos']
        data['bpm_5RDOROS_B2vPos'] = olddata['5rdoros_b2vpos']
        data['bpm_5RDOROS_B2hErr'] = olddata['5rdoros_b2herr']
        data['bpm_5RDOROS_B2vErr'] = olddata['5rdoros_b2verr']
        data['totsize'] = olddata['totsize']
        data['publishnnb'] = olddata['publishnnb']
        data['datasourceid'] = olddata['datasourceid']
        data['algoid'] = olddata['algoid']
        data['channelid'] = olddata['channelid']
        data['payloadtype'] = olddata['payloadtype']
    if 'bstar5' in olddata:
        data['bstar5'] = olddata['bstar5']
        data['xingHmurad'] = olddata['xingHmurad']

    return data


if __name__ == '__main__':
    '''Take all VdM DIP files from 2016 with the correct data format and
        configure analysis for them. You can also choose a specific fill'''
    logging.basicConfig(filename=default_folder + "Automation/Logs/Configurator_" + dt.datetime.now().strftime(
        '%y%m%d%H%M%S') + '.log', level=logging.DEBUG)
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-f', '--fill', help='only used for testing purposes', type=int)
    parser.add_argument('-d', '--dip', help='folder with dip file', default='/brildata/vdm/')
    parser.add_argument('-c', '--central', help='central folder', default='/brildata/17/')

    args = parser.parse_args()
    fillnum = args.fill
    dipdir = args.dip
    central = args.central
    dips = os.listdir(dipdir)
    for dip in dips:
        if dip[-4:] == '.csv' and os.path.getmtime(dipdir + dip) > 1460419200:
            data = pd.read_csv(dipdir + dip)
            if data.shape[0] != 0:
                gb = data.groupby('fill')
                for fill, group in gb:
                    if fillnum and fill == fillnum:
                        times = []

                        def timestamp(i): return dt.datetime.fromtimestamp(
                            group.iloc[i]['sec']).strftime('%y%b%d_%H%M%S')
                        name = str(fill) + '_' + timestamp(0) + \
                            '_' + timestamp(-1)
                        try:
                            times, steps = GetTimestamps(group, fill, name)
                        except:
                            message = '\n\t' + 'Error getting timestamps!\n' + traceback.format_exc()
                            print(message)
                            logging.error('\n\t' + dt.datetime.now().strftime(
                                '%d %b %Y %H:%M:%S\n') + '\nFill ' + str(fill) + '\nVdM file: ' + dip + message)
                        if times:
                            # for lum,fit,rate in zip(luminometers, fits,
                            # ratetables):
                            for i in range(len(fits)):
                                try:
                                    if os.path.exists(central+str(fill)):
                                        ConfigDriver(
                                            times, fill, luminometers[i], fits[i], ratetables[i], name, central + str(fill), not i)
                                    else:
                                        ConfigDriver(
                                            times, fill, luminometers[i], fits[i], ratetables[i], name, central, not i)
                                except:
                                    message = 'Error making config files\n' + traceback.format_exc()
                                    print(message)
                                    logging.error('\n\t' + dt.datetime.now().strftime(
                                        '%d %b %Y %H:%M:%S\n') + '\nFill ' + str(fill) + '\nVdM file: ' + dip + message)
