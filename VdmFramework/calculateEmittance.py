import pandas as pd
import numpy as np
import sys
from inputDataReaderII import *
import tables
import collections

def CalculateEmittance(Config, fitresultsFile):

    #filename=Config
    filename=Config["Filename"]

    gama=6930
    temp=0

    CapandLenX={}
    CapY={}

    sigmaZ1=[]
    sigmaZ2=[]
    
    emittanceX={}
    emittanceY={}

    sigmaX={}
    sigmaY={}

    b1filter=[]
    b2filter=[]

    h5file = tables.open_file(filename)
    
    vdmscan =h5file.root.vdmscan
    try:
    	bstar5= [r['bstar5'] for r in vdmscan]
    	xingHmurad= [r['xingHmurad'] for r in vdmscan]
	bstar_m = bstar5[0]*0.01
    	xingH = xingHmurad[0]*0.000001
    except:
	bstar_m= Config["BetaStar"]
	xingH= Config["Angle"]

    #print "bstar5[m] = ", bstar5[0]*0.01, ", xingHmurad = ", xingHmurad[0]

    beam =h5file.root.beam
    collidable = [r['collidable'] for r in beam]
    bxconfig1 = [r["bxconfig1"] for r in beam] 
    bxconfig2 = [r["bxconfig2"] for r in beam]

    for nb in range(len(collidable[0])):
	if collidable[0][nb]==1 and bxconfig1[0][nb]==1:
	    b1filter.append(1)
	elif collidable[0][nb]==0 and bxconfig1[0][nb]==1:
            b1filter.append(0)
    for nb in range(len(collidable[0])):
        if collidable[0][nb]==1 and bxconfig2[0][nb]==1:
            b2filter.append(1)
        elif collidable[0][nb]==0 and bxconfig2[0][nb]==1:
            b2filter.append(0)

    isBunchLength=True
    try:
        bunchlength = h5file.root.bunchlength
        b1length = [r['b1length'] for r in bunchlength]
        b2length = [r['b2length'] for r in bunchlength]
    except Exception, e:
        print >> sys.stderr, "Exception: %s" % str(e)
        isBunchLength=False

    h5file.close()

    df=pd.read_csv(fitresultsFile)
    bcid= df['BCID']
  
    for n, kb in enumerate(bcid):
        if kb < temp:
            keybcid = n
            break
        temp=kb
    
    capsigmaX = df['CapSigma'][:keybcid]
    capsigmaY = df['CapSigma'][keybcid:]
	
    if isBunchLength:
        b1length = np.mean(np.asarray(b1length), axis=0)
        b2length = np.mean(np.asarray(b2length), axis=0)
        b1length=np.trim_zeros(b1length,'b')
        b2length=np.trim_zeros(b2length,'b')

        b1length=np.multiply(np.asarray(b1filter),b1length)
        b1length = b1length[b1length != 0]
        b2length=np.multiply(np.asarray(b2filter),b2length)
        b2length = b2length[b2length != 0]

        b1length=(b1length*300000000)/4	
        b2length=(b2length*300000000)/4

        data_tuples=list(zip(bcid,b1length,b2length))

        aveLength = np.add(b1length,b2length)/2

    for i, bcid in enumerate(bcid):
        if i<keybcid:
	    if isBunchLength:
                CapandLenX[bcid]={'length': aveLength[i], 'capsigma':capsigmaX[i]*1E-03}
	    elif not isBunchLength:
		CapandLenX[bcid]={'capsigma':capsigmaX[i]*1E-03}
        else:
            CapY[bcid]={'capsigma':capsigmaY[i]*1E-03}
    if isBunchLength:
        for k,v in CapandLenX.items():
            emittanceX[k] = ( (v['capsigma']*v['capsigma']*gama) - (2*gama*v['length']*v['length']*np.sin(xingH)*np.sin(xingH)) ) / ( 2*bstar_m*np.cos(xingH)*np.cos(xingH) ) *1E6
	    sigmaX[k] = np.sqrt( ( (v['capsigma']*v['capsigma']) - (2*v['length']*v['length']*np.sin(xingH)*np.sin(xingH)) ) / ( 2*np.cos(xingH)*np.cos(xingH) ) ) *1E3
    elif not isBunchLength:
	for k,v in CapandLenX.items():
	    emittanceX[k] = (v['capsigma']*v['capsigma']*gama) / (2*bstar_m) *1E6
	    sigmaX[k] = np.sqrt( (v['capsigma']*v['capsigma']) / (2) ) *1E3
    for k,v in CapY.items():
        emittanceY[k] = (v['capsigma']*v['capsigma']*gama) / (2*bstar_m) *1E6
	sigmaY[k] = np.sqrt( (v['capsigma']*v['capsigma']) / (2) ) *1E3

    odEmitX=collections.OrderedDict(sorted(emittanceX.items()))
    odEmitY=collections.OrderedDict(sorted(emittanceY.items()))
    emittance = odEmitX.values()+odEmitY.values()
    
    odSigmaX=collections.OrderedDict(sorted(sigmaX.items()))
    odSigmaY=collections.OrderedDict(sorted(sigmaY.items()))
    sigma = odSigmaX.values()+odSigmaY.values()
	
    if isBunchLength:
        beamlength = pd.DataFrame(data_tuples, columns=['BCID','B1Length_m','B2Length_m'])
    elif not isBunchLength:
	beamlength=pd.DataFrame()

    return beamlength, emittance, sigma

    #df.insert(len(list(df))-4,'Emittance', od)
    #df.to_csv(fitresultsFile)


if __name__ == '__main__':

    #for testing purposes only
    a="/scratch/yusufcan_VdM/VdmFramework/Automation/Analysed_Data/6611_25Apr18_064615_25Apr18_064854/PLT/results/noCorr/SG_FitResults.csv"
    CalculateEmittance("/brildata/vdmdata18/6611_1804250157_1804250459.hd5",a )
