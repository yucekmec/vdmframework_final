#! /bin/bash


#filearr=/brildata/vdmdata17/6016/*
filearr=('6016_1707280758_1707280928.hd5' '6016_1707280946_1707281036.hd5' '6016_1707281040_1707281130.hd5' '6016_1707281237_1707281324.hd5' '6016_1707281922_1707282005.hd5');
#filearr=('6016_1707280356_1707280409.hd5') #EmittanceScan
#filearr=('6016_1707280946_1707281036.hd5')


:<<"joker"
comm02='python AutoAnalysis.py -t -bg -pdf -f /brildata/vdmdata17/6016/'

for file02 in ${filearr[@]}
do
	eval "${comm02}${file02}"
done

joker
#:<<"all"
#------------------------------------------------------

comm02='python AutoAnalysis.py -t -pdf -bg -od -b -f /brildata/vdmdata17/6016/'

for file02 in ${filearr[@]}
do
	echo ${file02}
	eval "${comm02}${file02}"
done
#:<<"SG"
#------------------------------------------------------

comm02='python AutoAnalysis.py -t -pdf -bg -od -b -db -f /brildata/vdmdata17/6016/'

for file02 in ${filearr[@]}
do
	eval "${comm02}${file02}"
done

#------------------------------------------------------

comm02='python AutoAnalysis.py -t -pdf -bg -od -b -db -ls -f /brildata/vdmdata17/6016/'

for file02 in ${filearr[@]}
do
	eval "${comm02}${file02}"
done

:<<"DG"
#------------------------------------------------------

comm02='python AutoAnalysis.py -t -pdf -d -bg -od -b -f /brildata/vdmdata17/6016/'

for file02 in $filearr
do
	eval "${comm02}${file02}"
done


#------------------------------------------------------

comm02='python AutoAnalysis.py -t -pdf -d -bg -od -b -db -f /brildata/vdmdata17/6016/'

for file02 in $filearr
do
	eval "${comm02}${file02}"
done

#------------------------------------------------------

comm02='python AutoAnalysis.py -t -pdf -d -bg -od -b -db -ls -f /brildata/vdmdata17/6016/'

for file02 in $filearr
do
	eval "${comm02}${file02}"
done

SG

all
