## Preparation of json for DynamicBeta correction
import sys, json, csv, pickle, re, math, os
import numpy as np
from scipy.interpolate import interp1d
from array import array

from inputDataReaderDynBeta import*

############ Function to read the table from file basetable
def readBaseTable(basetable):

    ## columns of the table in basetable
    normBeamSep=[]
    DeltaBSXwrtBSX_Xscan=[]
    DeltaBSYwrtBSY_Xscan=[]
    DeltaBSXwrtBSX_Yscan=[]
    DeltaBSYwrtBSY_Yscan=[]

    ## reading data line by line
    pattern=re.compile(r"[\t,\n]")
    i=0
    for line in basetable:
        i=i+1
        if(i>5):
            numlist=pattern.split(line)
            normBeamSep.append(float(numlist[0]))
            DeltaBSXwrtBSX_Xscan.append(float(numlist[1]))
            DeltaBSYwrtBSY_Xscan.append(float(numlist[2]))
            DeltaBSXwrtBSX_Yscan.append(float(numlist[3]))
            DeltaBSYwrtBSY_Yscan.append(float(numlist[4]))

    ## Dictionary with data from basetable
    SimDict={}
    SimDict['normBeamSep']=normBeamSep
    SimDict['DeltaBSXwrtBSX_Xscan']=DeltaBSXwrtBSX_Xscan
    SimDict['DeltaBSYwrtBSY_Xscan']=DeltaBSYwrtBSY_Xscan
    SimDict['DeltaBSXwrtBSX_Yscan']=DeltaBSXwrtBSX_Yscan
    SimDict['DeltaBSYwrtBSY_Yscan']=DeltaBSYwrtBSY_Yscan

    return SimDict

############ Correction curves for colliding bcid, ScanX, ScanY
def defineCorrectionCurves(Nsim,emitXsim,emitYsim,refidx,simdict,ScanX,ScanY,bcid):

    NX=ScanX.averCurrPerBX[bcid]
    NY=ScanY.averCurrPerBX[bcid]
    emitX=ScanX.emitPerBX[bcid]
    emitY=ScanY.emitPerBX[bcid]
    if ScanX.beamLength:
        sigmaX=np.sqrt( ((ScanX.CapSigmaPerBX[bcid]**2) - (2*(ScanX.beamLength[bcid]**2)*(np.sin(ScanX.angle)**2))) / (2*np.cos(ScanX.angle)**2)  )

        KX=NX/Nsim*math.sqrt(emitXsim)*(math.sqrt(emitXsim)+math.sqrt(emitYsim))/math.sqrt(emitX)/(math.sqrt(emitX)+math.sqrt(emitY))/np.sqrt(1+((ScanX.beamLength[bcid]/sigmaX)*np.tan(ScanX.angle))**2)
    elif not ScanX.beamLength:
	KX=NX/Nsim*math.sqrt(emitXsim)*(math.sqrt(emitXsim)+math.sqrt(emitYsim))/math.sqrt(emitX)/(math.sqrt(emitX)+math.sqrt(emitY))

    KY=NY/Nsim*math.sqrt(emitYsim)*(math.sqrt(emitXsim)+math.sqrt(emitYsim))/math.sqrt(emitY)/(math.sqrt(emitX)+math.sqrt(emitY))

    BSXrefwrtBSX0_Xscan=KX*simdict['DeltaBSXwrtBSX_Xscan'][refidx]+1.0
    BSYrefwrtBSY0_Xscan=KY*simdict['DeltaBSYwrtBSY_Xscan'][refidx]+1.0
    BSXrefwrtBSX0_Yscan=KX*simdict['DeltaBSXwrtBSX_Yscan'][refidx]+1.0
    BSYrefwrtBSY0_Yscan=KY*simdict['DeltaBSYwrtBSY_Yscan'][refidx]+1.0
   
    Length=len(simdict['DeltaBSXwrtBSX_Xscan'])
    BSXwrtBSXref_Xscan=[]
    BSYwrtBSYref_Xscan=[]
    BSXwrtBSXref_Yscan=[]
    BSYwrtBSYref_Yscan=[]

    for idx in range(Length):
        val=(KX*simdict['DeltaBSXwrtBSX_Xscan'][idx]+1.0)/BSXrefwrtBSX0_Xscan
        BSXwrtBSXref_Xscan.append(val)
        val=(KY*simdict['DeltaBSYwrtBSY_Xscan'][idx]+1.0)/BSYrefwrtBSY0_Xscan
        BSYwrtBSYref_Xscan.append(val)
        val=(KX*simdict['DeltaBSXwrtBSX_Yscan'][idx]+1.0)/BSXrefwrtBSX0_Yscan
        BSXwrtBSXref_Yscan.append(val)
        val=(KY*simdict['DeltaBSYwrtBSY_Yscan'][idx]+1.0)/BSYrefwrtBSY0_Yscan
        BSYwrtBSYref_Yscan.append(val)

    CorrectionCurves=[BSXwrtBSXref_Xscan,BSYwrtBSYref_Xscan,BSXwrtBSXref_Yscan,BSYwrtBSYref_Yscan]
    
#    if bcid=='1072':
#        print 'KX=', KX, 'KY=', KY
#        print 'NX=', NX, 'NY=', NY
#        print 'emitX=', emitX, 'emitY=', emitY
#        print CorrectionCurves[0]
#        print CorrectionCurves[1]
#        print CorrectionCurves[2]
#        print CorrectionCurves[3]

    return CorrectionCurves

############################################
## find DynamicBeta at scanpoints (per BX)
def interpolateDynBeta(Table,Scan,CorrCurves):
   
    normBeamSep=Table['normBeamSep']
    maxval=max(normBeamSep)

    collBunches=Scan.usedCollidingBunches
    DynBetaPerBX={}
    for bx in collBunches:
        CorrCurve=CorrCurves[bx]
        interpolateDB=interp1d(normBeamSep,CorrCurve)
        Shift=math.sqrt(2.0)*Scan.meanPerBX[bx]/Scan.CapSigmaPerBX[bx]
        SPList=Scan.normBeamSepPerBX_BBcorr[bx]
        Length=len(SPList)
        DynBeta=[]
        for sp in range(Length):
            coord=math.fabs(SPList[sp]-Shift)
            if coord<=maxval:
                val=interpolateDB(coord)
                val1=val.tolist()
                pair=(sp,val1)
                DynBeta.append(pair)
        if len(DynBeta)<Length:
            print "Warning from interpolateDynBeta: there were points out of the interpolation range, Scan", Scan.scanNumber, ", bx=", bx
            #print "bx=", bx, "length=", len(DynBeta)

        DynBetaPerBX[bx]=DynBeta

    return DynBetaPerBX

###############################
## find lumi correction factor per bx
def calculateLumiCorrFactor(Scan,DynBeta1,DynBeta2):

    collBunches=Scan.usedCollidingBunches
    LumiCorrFactorPerBX={}
    for bx in collBunches:
        dynbeta1=DynBeta1[bx] 
        dynbeta2=DynBeta2[bx]
        splist=Scan.normBeamSepPerBX_BBcorr[bx]

        #fast check
        Length1=len(dynbeta1)
        Length2=len(dynbeta2)
        #print "bx=", bx, "Length1=", Length1, "Length2=", Length2
        if Length1!=Length2:
            print "Error in calculateLumiCorrFactor: exit program"
            sys.exit()
        for idx in range(Length1):
            db1=dynbeta1[idx] 
            db2=dynbeta2[idx]
            if db1[0]!=db2[0]:
                print "Error in calcLumiCorrFactor: exit program"
                sys.exit()
       
        try: 
            LumiCorrFactor=[]
            for idx in range(Length1):

                db1=dynbeta1[idx]
                db2=dynbeta2[idx]
                #print 'bx=', bx, 'db1=', db1[1], 'db2=', db2[1]
                spidx=db1[0]
                coord=splist[spidx]
                val1=1.0/db1[1]
                val=math.sqrt(db1[1]*db2[1])*math.exp(-0.25*coord*coord*(1.0-val1))
                pair=(spidx,val)
                LumiCorrFactor.append(pair)
        except ValueError:
             print "bx=", bx, " is omitted in calculateLumiCorrFactor"
        else:
            LumiCorrFactorPerBX[bx]=LumiCorrFactor    

    return LumiCorrFactorPerBX

############ Main function to calculate the correction factor
def doMakeDynamicBetaFile(ConfigInfo,BaseTableFile):

    ## load simulated table
    #if base table is changed, this code should be overwritten from here -->

    #normalized emittance in um*rad  
    emitXsim=4.0
    emitYsim=4.0
    #intensity of opposing bunch, proton/bunch
    Nsim=8.5E10

    #simtable=open(r"DynamicBetaInfo/dynBetaCrctnTable_v1.0_22May13.txt","rt")
    simtable=open(BaseTableFile,"rt")
    SimDict=readBaseTable(simtable)
    simtable.close()

    #print SimDict

    #<-- till here (may be, together with readBaseTable())

    #vdm data reading
    Fill=ConfigInfo['Fill']
    AnalysisDir=ConfigInfo['AnalysisDir']
    Luminometer=ConfigInfo['Luminometer']
    OutputDir=AnalysisDir + '/' + ConfigInfo['OutputSubDir']

    inputBeamLengthFile = AnalysisDir + '/cond/BeamLength.csv'

    inputScanFile = AnalysisDir + '/' + str(ConfigInfo['InputScanFile'])
    inputBeamCurrentFile = AnalysisDir + '/' + str(ConfigInfo['InputBeamCurrentFile'])
    inputRatesFile = AnalysisDir + '/'+ str(ConfigInfo['InputLuminometerData'])
    inputCapSigmaFile=AnalysisDir + '/' + str(Luminometer)+ '/results/' + str(ConfigInfo['InputCapSigmaFile'])
    inputBeamBeamFile=AnalysisDir + '/' +str(ConfigInfo['InputBeamBeamFile'])
    Scanpairs=ConfigInfo['Scanpairs']
    FOM=ConfigInfo['FOM']
    if FOM==True:
        inputRatesFile = AnalysisDir + '/'+ str(ConfigInfo['FOM_InputLuminometerData'])
	inputCapSigmaFile=AnalysisDir + '/' + str(Luminometer)+ '/results/' + str(ConfigInfo['FOM_InputCapSigmaFile'])
	inputBeamBeamFile=AnalysisDir + '/' +str(ConfigInfo['FOM_InputBeamBeamFile'])
    #print inputScanFile
    #print inputBeamCurrentFile
    #print inputRatesFile
    #print inputCapSigmaFile

    inData1 = vdmInputData_DynBeta(1)
    inData1.GetScanInfo(inputScanFile)
    inData1.GetBeamCurrentsInfo(inputBeamCurrentFile)
    inData1.GetLuminometerData(inputRatesFile)
    inData1.GetBeamBeamData(inputBeamBeamFile)
    inData1.GetFitResults(inputCapSigmaFile)

    Fill = inData1.fill
    inData=[]
    inData.append(inData1)
    
    # for the remaining scans
    for i in range(1,len(inData1.scanNamesAll)):
        inDataNext = vdmInputData_DynBeta(i+1)
        inDataNext.GetScanInfo(inputScanFile)
        inDataNext.GetBeamCurrentsInfo(inputBeamCurrentFile)
        inDataNext.GetLuminometerData(inputRatesFile)
        inDataNext.GetBeamBeamData(inputBeamBeamFile)
        inDataNext.GetFitResults(inputCapSigmaFile)
        inData.append(inDataNext)
        
    for entry in inData:
        entry.applyBeamBeam()
        entry.averageCurrent()
        entry.normalizeBeamSeparation()
        entry.calculateEmittance(inputBeamLengthFile)

    
    #print inData[0].CapSigmaPerBX

## correction curves

    BSXwrtBSX_ref=[{} for s in range(len(inData))]
    BSYwrtBSY_ref=[{} for s in range(len(inData))]

    ## if reference point or base table is changed, check next lines. From here -->
    # reference point
    refidx=SimDict['normBeamSep'].index(0.0)
    for entry in Scanpairs:
        numX=entry[0]
        numY=entry[1]
        ScanX=inData[numX-1]
        ScanY=inData[numY-1]
        collBunches=ScanX.usedCollidingBunches
        for bx in collBunches:
            CorrCurves=defineCorrectionCurves(Nsim,emitXsim,emitYsim,refidx,SimDict,ScanX,ScanY,bx)
            BSXwrtBSX_ref[numX-1][bx]=CorrCurves[0]
            BSXwrtBSX_ref[numY-1][bx]=CorrCurves[2]
            BSYwrtBSY_ref[numX-1][bx]=CorrCurves[1]
            BSYwrtBSY_ref[numY-1][bx]=CorrCurves[3]
    ## <-- till here

## luminosity correction factor and output

    table={}
    csvtable=[]
    csvrow=['bx','(scanpoint,correction_factor)']
    csvtable.append(csvrow)

    for idx in range(len(inData)):
        Scan=inData[idx]
        DynBetaX=interpolateDynBeta(SimDict,Scan,BSXwrtBSX_ref[idx])
        DynBetaY=interpolateDynBeta(SimDict,Scan,BSYwrtBSY_ref[idx])

        #scanNumber = Scan.scanNumber
        scancaption = 'Scan_'+str(Scan.scanNumber)+'_'+str(Scan.scanName)
        csvrow=[str(scancaption)]
        csvtable.append(csvrow)

        if 'X' in Scan.scanName:
            LumiCorrFactor=calculateLumiCorrFactor(Scan,DynBetaX,DynBetaY)
        if 'Y' in Scan.scanName:
            LumiCorrFactor=calculateLumiCorrFactor(Scan,DynBetaY,DynBetaX)

        table[str(scancaption)]=LumiCorrFactor
        for bx in LumiCorrFactor:
            csvrow=[bx,LumiCorrFactor[bx]]
            csvtable.append(csvrow)
        
    #for bx in LumiCorrFactor.keys():
    #    print LumiCorrFactor[bx]

    #table={"correction":1.0759388884735564e-06}
    #csvrow=[{'correction':1.0759388884735564e-06}]
    #csvtable.append(csvrow)
    
    return table, csvtable

if __name__=='__main__':
    #read config file
    ConfigFile = sys.argv[1]
    #print "we are here"
    Config=open(ConfigFile)
    ConfigInfo=json.load(Config)
    Config.close()

    Fill=ConfigInfo['Fill']
    AnalysisDir=ConfigInfo['AnalysisDir']
    Luminometer=ConfigInfo['Luminometer']
    OutputDir=AnalysisDir + '/' + ConfigInfo['OutputSubDir']
    BaseTableFile="DynamicBetaInfo/dynBetaCrctnTable_v1.0_22May13.txt"

    table={}
    csvtable=[]

    table, csvtable=doMakeDynamicBetaFile(ConfigInfo,BaseTableFile)

    #print OutputDir+'/DynamicBeta_'+Luminometer+'_'+str(Fill)

    with open(OutputDir+'/DynamicBeta_'+Luminometer+'_'+str(Fill)+'.json','wb') as f:
        json.dump(table,f)

    csvfile=open(OutputDir+'/DynamicBeta_'+Luminometer+'_'+str(Fill)+'.csv','wb')
    writer=csv.writer(csvfile)
    writer.writerows(csvtable)
    csvfile.close()
